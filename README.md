This project uses:
- [Bazel](https://bazel.build/) to build the application
- [Bazel](https://bazel.build/) and [gitlab](https://about.gitlab.com/) to deploy the application

Please find:
- Application source code and documentation under [services/apps/farmer](services/apps/farmer) directory
- Tools for build and deployment under [tools](tools) directory. This folder contains bazel resources to create env and build dependencies. It also contains shell scripts needed by ci to build and deploy the application.
- Manifests for deployment under [manifests](manifests) directory. This folder contains all manifests needed by bazel to make a custom deployment of the application.


Note: For now, only single page application is built and deployed by Bazel.


# Development

## Build locally with bazel

If your machine is not a linux one, you need to update node binary used by bazel. Please set it in [build.sh file](tools/ci/build.sh). Then run:

```shell
make build
```


## Updated files in tools folder

These files are updated and are specific to this project:
- [services](services)
- [manifests](manifests)
- [tools/ci/build.sh](tools/ci/build.sh)
- [tools/hack/get_workspace_status.sh](tools/hack/get_workspace_status.sh)
- [tools/hack/update_bazel.sh](tools/hack/update_bazel.sh)
- [tools/repositories/deps-step-1.bzl](tools/repositories/deps-step-1.bzl)
- [tools/repositories/deps-step-2.bzl](tools/repositories/deps-step-2.bzl)


## Single page application

### Generate docker image with bazel

First you need to build SPA:
- Without bazel by following directives into [this README](services/apps/farmer/docs/getting-started.md)
- With bazel (Build locally with bazel)


Once the SPA is built, run following commands:
```shell
# Go to the source folder
cd services/apps/farmer
bazel run :image
```

### Run Docker image locally

As the nginx.conf is injected at run time by k8s, if you need to test/run the image locally you will need to manually mount nginx.conf into your container.

```shell
# Please note that the docker image is printed by bazel at the end of generation phase
docker run -it --rm -p 8080:80 -v /absolute-path-to-src/farmer-mobile-app/services/apps/farmer/deploy/base/nginx.conf:/etc/nginx/nginx.conf bazel/services/apps/farmer:image
```