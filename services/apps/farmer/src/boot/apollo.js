import { createApolloProvider } from '@vue/apollo-option'
import createApolloClient from 'src/graphql/apollo/client'

const apolloClient = createApolloClient()
const apolloProvider = createApolloProvider({
  defaultClient: apolloClient
})

export default ({ app }) => {
  app.use(apolloProvider)
}

export { apolloClient, apolloProvider }
