import { Notify } from 'quasar'

Notify.setDefaults({
  actions: [{ icon: 'las la-times', color: 'white' }],
  progress: true,
  position: 'top'
})
