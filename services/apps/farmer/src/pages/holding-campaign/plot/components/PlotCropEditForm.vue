<template>
  <q-form @submit="onSubmit">
    <q-card
      :square="!isDesktopPlatform"
      class="column no-wrap col"
    >
      <q-card-section>
        <q-select
          v-model="selectedPlantSpecies"
          :options="plantSpeciesOptions"
          option-value="id"
          :option-label="getPlantSpeciesOptionLabel"
          filled
          @filter="filterPlantSpecies"
          use-input
          input-debounce="200"
          :loading="loadingPlantSpecies"
          :label="$gettext('Crop')"
          :rules="[ val => Boolean(val) || $gettext('Select a crop')]"
        />
        <q-select
          v-if="showPlantVarietySelect"
          v-model="selectedPlantVarietyId"
          emit-value
          map-options
          :options="plantVarietiesOptions"
          option-value="id"
          option-label="name"
          filled
          :loading="loadingPlantVarieties"
          :label="$gettext('Crop variety')"
          :rules="[
            val => Boolean(val) || $gettext('Select a crop variety'),
            val => isPlantVarietyAvailable(val) || $gettext('This crop variety is already used, select another one.')
          ]"
          class="q-mt-md"
        />
        <template v-if="plotPlantVarietyMultipleEnabled">
          <q-input
            v-model.number="percentage"
            :hide-bottom-space="showCropAreaPercentageWarning"
            filled
            type="number"
            :label="$gettext('Plot area used by the crop')"
            suffix="%"
            :min="0.1"
            :max="100"
            step="any"
            :rules="[ val => val && val >= 1 && val <= 100 || $gettext('Enter a positive number up to 100%')]"
            class="q-mt-md"
          />
          <div
            v-if="showCropAreaPercentageWarning"
            class="q-pb-sm text-warning text-caption"
          >
            <q-icon name="las la-exclamation-triangle" />
            <translate>
              Total crops percentage is above 100%
            </translate>
            ({{ (100 - maxPercentage + percentage) }}%)
          </div>
        </template>
        <q-input
          v-model.number="cropYieldInput"
          filled
          type="number"
          :label="$gettext('Crop yield (optional)')"
          :suffix="cropYieldDisplayUnit"
          :min="0"
          step="any"
          :rules="[ val => !isNaN(val) || val < 0 || $gettext('Enter a valid crop yield')]"
          class="q-mt-md"
        />
        <q-select
          v-if="hasIrrigationMethods"
          v-model="selectedIrrigationMethod"
          :options="irrigationMethods"
          option-value="id"
          :option-label="geIrrigationMethodOptionLabel"
          clearable
          clear-icon="las la-times-circle"
          filled
          :loading="$apollo.queries.irrigationMethods.loading"
          :label="$gettext('Irrigation method (optional)')"
          class="q-mt-md"
        />
      </q-card-section>

      <q-card-section class="q-py-lg">
        <div class="row justify-end items-center">
          <q-btn
            :label="$gettext('Cancel')"
            flat
            @click="onCancel"
          />
          <q-btn
            :label="$gettext('Save')"
            color="primary"
            type="submit"
          />
        </div>
      </q-card-section>
    </q-card>
  </q-form>
</template>

<script>
import { formatPlotPlantVarietyWithFallback, isPlotPlantVarietyNameValid } from 'src/services/plot/plotFormatting'
import { PlatformMixin } from 'src/mixins/PlatformMixin'
import queryPlantSpecies from 'src/pages/holding-campaign/plot/graphql/queryPlantSpecies'
import queryPlantVarietiesByPlantSpecies from 'src/pages/holding-campaign/plot/graphql/queryPlantVarietiesByPlantSpecies'
import queryIrrigationMethods from 'src/pages/holding-campaign/plot/graphql/queryIrrigationMethods'
import featureFlags from 'src/services/feature-flags'
import { getI18nField } from 'src/services/translate'
import { getYieldDisplayUnit, getSurfaceConversionRatio } from 'src/services/display-surface-unit'

export default {
  name: 'PlotCropEditForm',
  mixins: [
    PlatformMixin
  ],
  props: {
    initialPlantSpecies: {
      type: Object,
      default: null
    },
    initialPlantVariety: {
      type: Object,
      default: null
    },
    initialIrrigationMethod: {
      type: Object,
      default: null
    },
    initialPercentage: {
      type: Number,
      required: true
    },
    maxPercentage: {
      type: Number,
      required: true
    },
    initialCropYield: {
      type: Number,
      default: null
    },
    existingPlotPlantVarieties: {
      type: Array,
      default: null
    }
  },
  emits: ['on-save', 'on-cancel'],
  data () {
    return {
      selectedPlantSpecies: this.initialPlantSpecies,
      selectedPlantVarietyId: this.initialPlantVariety?.id,
      selectedIrrigationMethod: this.initialIrrigationMethod,
      percentage: Math.min(100, this.initialPercentage),
      plantSpeciesFilterValue: null,
      cropYield: this.initialCropYield
    }
  },
  apollo: {
    plantSpecies: {
      query: queryPlantSpecies,
      notifyOnNetworkStatusChange: true,
      update: data => data.plant_species
    },
    plantVarieties: {
      query: queryPlantVarietiesByPlantSpecies,
      notifyOnNetworkStatusChange: true,
      variables () {
        return {
          plant_species_id: this.selectedPlantSpecies?.id
        }
      },
      update: data => data.plant_variety,
      skip () {
        return !this.selectedPlantSpecies
      }
    },
    irrigationMethods: {
      query: queryIrrigationMethods,
      notifyOnNetworkStatusChange: true
    }
  },
  computed: {
    plantSpeciesOptions () {
      const options = this.plantSpecies
      if (this.plantSpeciesFilterValue) {
        return options.filter(v => {
          return getI18nField(v, 'name')?.toLowerCase().indexOf(this.plantSpeciesFilterValue) > -1
        })
      } else {
        return options
      }
    },
    plantVarietiesOptions () {
      if (!this.plantVarieties) {
        return []
      }
      // keep only first of plant varieties whose name is invalid
      const varietiesOptions = []
      let invalidPlantVarietyFound = false
      for (const plantVariety of this.plantVarieties) {
        const isNameValid = isPlotPlantVarietyNameValid(plantVariety)
        if (isNameValid || !invalidPlantVarietyFound) {
          varietiesOptions.push({ id: plantVariety.id, name: formatPlotPlantVarietyWithFallback(plantVariety, this.selectedPlantSpecies) })
          if (!isNameValid) {
            invalidPlantVarietyFound = true
          }
        }
      }
      // sort alphabetically
      varietiesOptions.sort((a, b) => {
        if (a.name < b.name) {
          return -1
        } else if (a.name > b.name) {
          return 1
        } else {
          return 0
        }
      })
      return varietiesOptions
    },
    hasIrrigationMethods () {
      return this.irrigationMethods?.length
    },
    loadingPlantSpecies () {
      return !this.plantSpecies && this.$apollo.queries.plantSpecies.loading
    },
    loadingPlantVarieties () {
      return !this.plantVarieties && this.$apollo.queries.plantVarieties.loading
    },
    showCropAreaPercentageWarning () {
      return this.percentage > 0 && this.percentage <= 100 && this.percentage > this.maxPercentage
    },
    showPlantVarietySelect () {
      return this.plantVarietiesOptions?.length > 1
    },
    plotPlantVarietyMultipleEnabled () {
      return featureFlags.plotPlantVarietyMultipleEnabled()
    },
    cropYieldDisplayUnit () {
      return getYieldDisplayUnit()
    },
    cropYieldInput: {
      get () {
        if (!this.cropYield) {
          return null
        } else {
          const conversionRatio = getSurfaceConversionRatio()
          return this.cropYield * conversionRatio
        }
      },
      set (v) {
        let value = null
        if (v > 0) {
          const conversionRatio = getSurfaceConversionRatio()
          value = v / conversionRatio
        }
        this.cropYield = value
      }
    }
  },
  watch: {
    plantVarietiesOptions (varieties) {
      if (this.selectedPlantSpecies !== this.initialPlantSpecies) {
        // on plant species change, autoselect plant variety if applicable
        if (varieties?.length === 1) {
          this.selectedPlantVarietyId = varieties[0].id
        } else {
          this.selectedPlantVarietyId = null
        }
      }
    }
  },
  methods: {
    onSubmit () {
      if (this.selectedPlantVarietyId) {
        const percent = this.percentage ?? 100
        const cropYield = this.cropYield === '' ? null : this.cropYield
        this.$emit('on-save', this.selectedPlantVarietyId, this.selectedIrrigationMethod?.id, percent, cropYield)
      }
    },
    onCancel () {
      this.$emit('on-cancel')
    },
    filterPlantSpecies (val, update) {
      update(() => {
        this.plantSpeciesFilterValue = val.toLowerCase()
      })
    },
    isPlantVarietyAvailable (plantVarietyId) {
      return this.initialPlantVariety?.id === plantVarietyId || !this.existingPlotPlantVarieties?.find(plotPlantVariety => plotPlantVariety.plantVariety?.id === plantVarietyId)
    },
    getPlantSpeciesOptionLabel (option) {
      return getI18nField(option, 'name')
    },
    geIrrigationMethodOptionLabel (option) {
      return getI18nField(option, 'label')
    }
  }
}
</script>
