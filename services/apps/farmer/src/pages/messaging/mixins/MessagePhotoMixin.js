
const FAST_MESSAGE_GEO_TAGGED_PHOTO_PREFIX = '<<< FaST GeoTaggedPhoto'
const FAST_MESSAGE_GEOT_AGGED_PHOTO_SUFFIX = '>>>'

export default {

  methods: {
    isMessagePhoto (messageText) {
      return messageText && messageText.startsWith(FAST_MESSAGE_GEO_TAGGED_PHOTO_PREFIX) && messageText.endsWith(FAST_MESSAGE_GEOT_AGGED_PHOTO_SUFFIX)
    },
    getPhotoText (photo) {
      return `${FAST_MESSAGE_GEO_TAGGED_PHOTO_PREFIX} id=${photo.id} ${FAST_MESSAGE_GEOT_AGGED_PHOTO_SUFFIX}`
    }
  }

}
