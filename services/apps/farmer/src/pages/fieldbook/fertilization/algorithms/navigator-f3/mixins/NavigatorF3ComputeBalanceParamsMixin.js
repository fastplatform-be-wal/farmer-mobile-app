export default {
  methods: {
    computeBalanceParams (fertilizationPlanParameters, fertilizations) {
      const params = {}

      // Crop profile
      params.cropProfileId = fertilizationPlanParameters.cropProfile.id
      params.cropYield = fertilizationPlanParameters.cropTargetYield

      // Harvest and residues
      params.residuesLeftInField = fertilizationPlanParameters.residuesLeftInField
      params.harvestIndex = fertilizationPlanParameters.harvestIndex
      params.cv = fertilizationPlanParameters.harvestCV
      params.harvestNitrogenContent = fertilizationPlanParameters.harvestNitrogen
      params.harvestPhosphorusContent = fertilizationPlanParameters.harvestPhosphorus
      params.harvestPotassiumContent = fertilizationPlanParameters.harvestPotassium

      // PK strategy
      params.phosphorusPotassiumStrategy = fertilizationPlanParameters.pkStrategy?.name

      // Soil
      params.soilId = fertilizationPlanParameters.soilTexture.id
      params.soilPH = fertilizationPlanParameters.soilPH
      params.soilCEC = fertilizationPlanParameters.soilCEC
      params.soilOrganicMatterContent = fertilizationPlanParameters.soilOrganicMatter
      params.soilDepth = fertilizationPlanParameters.soilDepth
      params.soilNitrogenContentInitial = fertilizationPlanParameters.soilNitrogenInitialContent
      params.soilNitrogenContentFinal = fertilizationPlanParameters.soilNitrogenFinalContent
      params.soilPhosphorusAnalysisMethodId = fertilizationPlanParameters.soilPhosphorusAnalysisMethod.id
      params.soilPhosphorusContent = fertilizationPlanParameters.soilPhosphorusContent
      params.soilPotassiumContent = fertilizationPlanParameters.soilPotassiumContent

      // Irrigation
      params.climateZoneId = fertilizationPlanParameters.irrigationClimateZone.id
      params.rainfallAnnual = fertilizationPlanParameters.irrigationAnnualRainfall
      params.rainfallAutumnWinter = fertilizationPlanParameters.irrigationAutomnWinterRainfall
      params.waterSupplyMethod = fertilizationPlanParameters.irrigationWaterSupply?.name
      params.irrigationMethod = fertilizationPlanParameters.irrigationType?.name
      params.irrigationDose = fertilizationPlanParameters.irrigationDose
      params.irrigationNitrogenNo3Content = fertilizationPlanParameters.irrigationNitrogenContent

      // Fertilizations
      params.fertilizations = fertilizations.map(fertilization => ({
        fertilizer_id: fertilization.fertilizer?.id ?? null,
        quantity: fertilization.fertilizerQuantity,
        application_frequency: fertilization.fertilizerApplicationFrequency?.name,
        incorporation_method: fertilization.fertilizerIncorporationMethod?.name,
        name: fertilization.customName,
        is_organic: fertilization.customIsOrganic,
        nitrogen_total_content: fertilization.customIsOrganic ? null : fertilization.customNitrogenContent,
        nitrogen_organic_dry_matter_content: fertilization.customIsOrganic ? fertilization.customNitrogenContent : null,
        phosphorus_total_content: fertilization.customPhosphorusContent,
        potassium_total_content: fertilization.customPotassiumContent
      }))

      return params
    }
  }
}
