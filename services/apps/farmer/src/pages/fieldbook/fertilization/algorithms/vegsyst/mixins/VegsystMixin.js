import queryVegsystInputsControl from 'src/pages/fieldbook/fertilization/algorithms/vegsyst/graphql/queryVegsystInputsControl'

export default {
  data () {
    return {
      hasLoadedInputsControl: false,
      vegsystInputsControl: null
    }
  },
  computed: {
  },
  apollo: {
    vegsystInputsControl: {
      query: queryVegsystInputsControl,
      notifyOnNetworkStatusChange: true,
      // fetchPolicy: 'cache-first',
      update (data) {
        // transform the list of input controls to an object with input names as keys
        const inputsAsObject = {}
        if (data?.vegsyst__input_control?.length) {
          data.vegsyst__input_control.forEach(inputControl => { inputsAsObject[inputControl.id] = inputControl })
        }
        this.onInputControlQueryUpdate(inputsAsObject)
        this.hasLoadedInputsControl = true
        return inputsAsObject
      }
    }
  },
  methods: {
    onInputControlQueryUpdate (inputControls) {
      // use this function in components to do custom things when the vegsyst inputs control query has returned data.
      // E.g. set default values
    }
  }
}
