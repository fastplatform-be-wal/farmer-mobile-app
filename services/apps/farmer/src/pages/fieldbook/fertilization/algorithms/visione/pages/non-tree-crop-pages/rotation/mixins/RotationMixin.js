export default {
  computed: {
    isCancelable () {
      // determine if the current "new" rotation is cancelable.
      // the new rotation is cancelable only if the user clicks on the button "add new rotation" on the "rotations" page
      return this.$store.state.fertilizationPlan?.parameters?.new_rotation?.cancelable ?? false
    }
  }
}
