export default {
  methods: {
  },
  data () {
    return {
      irrigationMethods: [
        { value: 'DRY_RAINFED', label: this.$gettext('Dry rainfed') },
        { value: 'WET_RAINFED', label: this.$gettext('Wet rainfed') },
        { value: 'IRRIGATED', label: this.$gettext('Irrigated') }
      ],
      strategies: [
        { value: 'SUFFICIENCY', label: this.$gettext('Sufficiency strategy (minimum fertilizer)') },
        { value: 'REDUCED', label: this.$gettext('Build-up & maintenance (reduced fertilizer)') },
        { value: 'MAXIMUM', label: this.$gettext('Build-up & maintenance (maximum yield)') },
        { value: 'MAINTENANCE', label: this.$gettext('Maintenance (soil analysis not available)') }
      ],
      soilTypes: [
        { value: 'SANDY', label: this.$gettext('Sandy') },
        { value: 'SANDY_LOAM', label: this.$gettext('Sandy loam') },
        { value: 'LOAM', label: this.$gettext('Loam') },
        { value: 'SILTY_LOAM', label: this.$gettext('Silty loam') },
        { value: 'CLAY_LOAM', label: this.$gettext('Clay loam') },
        { value: 'CLAY', label: this.$gettext('Clay') }
      ],
      requiredSoilProperties: [
        {
          id: 'organic_matter_pct',
          label: this.$gettext('Percentage of organic matter'),
          unit_of_measure: {
            id: 'percentage',
            symbol: '%'
          }
        },
        {
          id: 'texture',
          label: this.$gettext('Soil texture'),
          unit_of_measure: {
            id: 'no_unit',
            symbol: '-'
          }
        },
        {
          id: 'phosphorus',
          label: this.$gettext('Phosphorus in soil'),
          unit_of_measure: {
            id: 'ppm',
            symbol: this.$pgettext('Part per Million', 'ppm')
          }
        },
        {
          id: 'potassium',
          label: this.$gettext('Potassium in soil'),
          unit_of_measure: {
            id: 'ppm',
            symbol: this.$pgettext('Part per Million', 'ppm')
          }
        }
      ],
      // inputs range control
      yieldMin: 1,
      yieldMax: 200000,
      pMin: 1,
      pMax: 1000,
      kMin: 1,
      kMax: 5000,
      organicMatterMin: 0,
      organicMatterMax: 20,
      nInWaterMin: 0,
      nInWaterMax: 1000
    }
  }
}

export const calculateNQuantityFromIrrigation = (nInWaterInPPM, irrigationDebitInM3PerHa) => {
  // nInWaterInPPM (ppm or mg/L) and irrigationDebitInM3PerHa (m3/ha)
  // nInWaterInKgPerL = (nInWaterInPPM / 1 000 000) kg/L
  // irrigationDebitInLPerHa = (irrigationDebitInM3PerHa * 1000) L/ha
  // n from irrigation (kg/ha) = nInWaterInKgPerL * irrigationDebitInLPerHa kg/ha
  return (Number(nInWaterInPPM) / 1000000) * (Number(irrigationDebitInM3PerHa) * 1000)
}
