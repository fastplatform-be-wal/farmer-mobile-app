import { getCurrentLanguage, getI18nFieldName } from 'src/services/translate'
import algorithms, { REQUAFERTIGRASSLAND } from 'src/services/fieldbook/fertilization/algorithms'

const requafertI18nLanguages = algorithms[REQUAFERTIGRASSLAND].i18nLanguages

export default {
  methods: {
    getRequafertiGrasslandI18NFieldName (fieldName) {
      if (requafertI18nLanguages.includes(getCurrentLanguage())) {
        return getI18nFieldName(fieldName)
      }
      return fieldName
    }
  }
}
