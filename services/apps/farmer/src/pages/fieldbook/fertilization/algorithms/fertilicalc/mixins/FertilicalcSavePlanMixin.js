export default {
  computed: {
    isFertilizationPlanUnsaved () {
      return this.$store.state.fertilizationPlan.fertilizationPlanIdAndName?.id == null
    }
  },
  methods: {
    getNPKRecommendations () {
      // npk recommendation
      return this.$store.state.fertilizationPlan.parameters.results.map(result => {
        return {
          rotation_order: result.rotation_index,
          plot_id: result.plot_id,
          plot_name: result.plot.name,
          plant_species_id: this.$store.state.fertilizationPlan.parameters.plant_species.id,
          plant_species_name: this.$store.state.fertilizationPlan.parameters.plant_species.name,
          crop_yield: this.$store.state.fertilizationPlan.parameters.yield,
          crop_yield_unit_of_measure_id: 'kilogram_per_hectare',
          element_recommendations: [
            {
              chemical_element_id: 'N',
              quantity: Number(Number(result.n).toFixed(1)),
              unit_of_measure_id: 'kilogram_per_hectare'
            },
            {
              chemical_element_id: 'P',
              quantity: Number(Number(result.p).toFixed(1)),
              unit_of_measure_id: 'kilogram_per_hectare'
            },
            {
              chemical_element_id: 'K',
              quantity: Number(Number(result.k).toFixed(1)),
              unit_of_measure_id: 'kilogram_per_hectare'
            }
          ],
          advices: [],
          warnings: []
        }
      })
    },
    prepareSavePlan () {
      const npkRecommendations = this.getNPKRecommendations()
      const results = {
        npk_recommendations: npkRecommendations
      }
      // basal configuration
      if (this.$store.state.fertilizationPlan.parameters?.fertilization_strategy_basal_final_configuration) {
        results.basal_configuration = { ...this.$store.state.fertilizationPlan.parameters.fertilization_strategy_basal_final_configuration }
      }
      // topdressing configuration
      if (this.$store.state.fertilizationPlan.parameters?.fertilization_strategy_topdressing_application) {
        results.topdressing_configuration = [...this.$store.state.fertilizationPlan.parameters.fertilization_strategy_topdressing_application]
      }
      return results
    }
  }
}
