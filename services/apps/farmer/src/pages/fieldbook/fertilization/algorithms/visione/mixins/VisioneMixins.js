import { resultTypes as _resultTypes, resultTypesLabel as _resultTypesLabel } from 'src/services/fieldbook/fertilization/fertilizationPlan'

export const PROFILE_GROUPS = {
  ARBOREE: 'arboree',
  ORTICOLE: 'orticole',
  ERBACEE: 'erbacee',
  FRUTTI_MINORI: 'frutti minori'
}
export const PRODUCTION_PHASES = [
  {
    id: 'establishment',
    label: 'Anno di impianto'
  },
  {
    id: 'year_1',
    label: 'Anno 1'
  },
  {
    id: 'year_2',
    label: 'Anno 2'
  },
  {
    id: 'production',
    label: 'Fase produttiva (produzione)'
  }
]
export const MANURE_DISTRIBUTIONS = [
  {
    id: 'annual',
    label: 'Ogni anno'
  },
  {
    id: 'previous_year_yes',
    label: 'Non ogni anno, ma lo scorso anno sì'
  },
  {
    id: 'previous_year_no',
    label: 'Non ogni anno, lo scorso anno no, oppure mai'
  }
]
export const MANURE_DISTRIBUTION_ID_TO_ASK_DETAILS_FOR = 'previous_year_yes'
export const USDA_TEXTURES = [
  {
    id: 'argilloso',
    label: 'Argilloso'
  },
  {
    id: 'argilloso_limoso',
    label: 'Argilloso Limoso'
  },
  {
    id: 'argilloso_sabbioso',
    label: 'Argilloso Sabbioso'
  },
  {
    id: 'franco_sabbioso',
    label: 'Franco Sabbioso'
  },
  {
    id: 'sabbioso',
    label: 'Sabbioso'
  },
  {
    id: 'sabbioso_franco',
    label: 'Sabbioso Franco'
  },
  {
    id: 'franco',
    label: 'Franco'
  },
  {
    id: 'franco_argilloso',
    label: 'Franco Argilloso'
  },
  {
    id: 'franco_limoso',
    label: 'Franco Limoso'
  },
  {
    id: 'franco_limoso_argilloso',
    label: 'Franco Limoso Argilloso'
  },
  {
    id: 'franco_sabbioso_argilloso',
    label: 'Franco Sabbioso Argilloso'
  },
  {
    id: 'limoso',
    label: 'Limoso'
  }
]
export const MAPPING_SOIL_TEXTURE_AND_SAND_CLAY_PERCENTAGES = {
  argilloso: {
    clay: 70,
    sand: 15
  },
  argilloso_limoso: {
    clay: 47,
    sand: 7
  },
  argilloso_sabbioso: {
    clay: 42,
    sand: 52
  },
  franco_sabbioso: {
    clay: 7,
    sand: 85
  },
  sabbioso: {
    clay: 3,
    sand: 94
  },
  sabbioso_franco: {
    clay: 10,
    sand: 63
  },
  franco: {
    clay: 20,
    sand: 40
  },
  franco_argilloso: {
    clay: 33,
    sand: 33
  },
  franco_limoso: {
    clay: 15,
    sand: 17
  },
  franco_limoso_argilloso: {
    clay: 33,
    sand: 10
  },
  franco_sabbioso_argilloso: {
    clay: 27,
    sand: 62
  },
  limoso: {
    clay: 4,
    sand: 8
  }
}

export default {
  methods: {
    cancelAddingNewCropRotation () {
      // REQUIRES AlgorithmMixins
      // reset the 'new_rotation' key in the store
      this.updateParameter('new_rotation', null)
      // redirect to the crop rotation page
      this.$router.replace({
        name: 'visioneCropRotations'
      })
    },
    plantSpeciesDisplayName (plantSpecies) {
      // plant_species is an object returned by a graphql request to the 'plant_species table'
      let s = plantSpecies.name
      if (plantSpecies.plant_species_group.name !== '-') {
        s += ' (' + plantSpecies.plant_species_group.name + ')'
      }
      return s
    },
    updateActualRotationParameter (rotationIndex, key, value) {
      // REQUIRES AlgorithmMixins
      let newParameters
      if (this.$store.state.fertilizationPlan.parameters.actualRotation &&
        this.$store.state.fertilizationPlan.parameters.actualRotation[rotationIndex]) {
        newParameters = { ...this.$store.state.fertilizationPlan.parameters.actualRotation[rotationIndex] }
      }
      newParameters[key] = value
      // call the updateParameter function of the AlgorithmMixins
      this.updateParameter(
        'actualRotation',
        [
          ...this.$store.state.fertilizationPlan.parameters.actualRotation.slice(0, rotationIndex),
          newParameters,
          ...this.$store.state.fertilizationPlan.parameters.actualRotation.slice(rotationIndex + 1)
        ]
      )
    },
    updateNewRotationParameter (key, value) {
      // REQUIRES AlgorithmMixins
      let newParameters = {}
      if (this.$store.state.fertilizationPlan.parameters.new_rotation) {
        newParameters = { ...this.$store.state.fertilizationPlan.parameters.new_rotation }
      }
      newParameters[key] = value
      // call the updateParameter function of the AlgorithmMixins
      this.updateParameter('new_rotation', newParameters)
    },
    updatePreviousRotationParameter (key, value) {
      // REQUIRES AlgorithmMixins
      let newParameters = {}
      if (this.$store.state.fertilizationPlan.parameters.previous_rotation) {
        newParameters = { ...this.$store.state.fertilizationPlan.parameters.previous_rotation }
      }
      newParameters[key] = value
      // call the updateParameter function of the AlgorithmMixins
      this.updateParameter('previous_rotation', newParameters)
    }
  },
  data () {
    return {
      resultTypes: _resultTypes,
      resultTypesLabel: _resultTypesLabel,
      soilProperties: [
        {
          id: 'organic_matter_pct',
          label: this.$gettext('Percentage of organic matter in soil'),
          unit_of_measure: {
            id: 'percentage',
            symbol: '%'
          }
        },
        {
          id: 'sand_pct',
          label: this.$gettext('Percentage of sand in soil'),
          unit_of_measure: {
            id: 'percentage',
            symbol: '%'
          }
        },
        {
          id: 'clay_pct',
          label: this.$gettext('Percentage of clay in soil'),
          unit_of_measure: {
            id: 'percentage',
            symbol: '%'
          }
        },
        {
          id: 'texture',
          label: this.$gettext('Soil texture'),
          unit_of_measure: {
            id: 'no_unit',
            symbol: '-'
          }
        },
        {
          id: 'ph',
          label: this.$gettext('pH'),
          unit_of_measure: {
            id: 'no_unit',
            symbol: '-'
          }
        },
        {
          id: 'phosphorus',
          label: this.$gettext('Phosphorus available as P Olsen in soil'),
          unit_of_measure: {
            id: 'ppm',
            symbol: this.$pgettext('Part per million', 'ppm')
          }
        },
        {
          id: 'potassium',
          label: this.$pgettext('Exchangeable K is the name of the chemical extraction method. In Italian: "K scambiabile"', 'Potassium available as exchangeable K in soil'),
          unit_of_measure: {
            id: 'ppm',
            symbol: this.$pgettext('Part per million', 'ppm')
          }
        },
        {
          id: 'c_n',
          label: this.$gettext('Ratio C/N (Carbon over Nitrogen)'),
          unit_of_measure: {
            id: 'no_unit',
            symbol: '-'
          }
        }
      ]
    }
  },
  computed: {
    availablePlantSpecies () {
      const data = []
      if (this.$store.state.fertilizationPlan?.target?.plot_plant_varieties?.length) {
        this.$store.state.fertilizationPlan.target.plot_plant_varieties.forEach(plotPlantVariety => {
          data.push(plotPlantVariety.plant_variety.plant_species)
        })
      }
      return data
    },
    canHaveMultipleRotations () {
      // The number of rotations allowed depends on the profile group:
      // - orticole: can have more than one rotation
      // - others: only one rotation
      if (this.$store.state.fertilizationPlan?.parameters?.current_rotations?.length) {
        // look if one of the rotations is an "orticole"
        return this.$store.state.fertilizationPlan.parameters.current_rotations.find(rotation => rotation.visione_profile?.profile_group === PROFILE_GROUPS.ORTICOLE) != null
      } else {
        return false
      }
    }
  }
}
