import { getCurrentLanguage, getI18nFieldName } from 'src/services/translate'
import algorithms, {REQUAFERTI, REQUAFERTI_PHOSPHATE} from 'src/services/fieldbook/fertilization/algorithms'

const requafertiPhosphateI18nLanguages = algorithms[REQUAFERTI_PHOSPHATE].i18nLanguages

export default {
  methods: {
    getRequafertiPhosphateI18NFieldName (fieldName) {
      if (requafertiPhosphateI18nLanguages.includes(getCurrentLanguage())) {
        return getI18nFieldName(fieldName)
      }
      return fieldName
    }
  }
}
