import mutationInsertAddOnSubscription from 'src/pages/add-ons/graphql/mutationInsertAddOnSubscription'
import mutationDeleteAddOnSubscriptionByPk from 'src/pages/add-ons/graphql/mutationDeleteAddOnSubscriptionByPk'
import { getI18nField } from 'src/services/translate'

export default {
  methods: {
    async subscribe (addOn) {
      try {
        const userCurrentCampaignHoldingId = this.$store.state.fastplatform.holdingCampaign.holding.id
        await this.$apollo.mutate({
          mutation: mutationInsertAddOnSubscription,
          variables: {
            object: {
              add_on_id: addOn.id,
              holding_id: userCurrentCampaignHoldingId
            }
          }
        })
        this.$q.notify({
          message: `${this.$gettext('You are now subscribed to')} ${getI18nField(addOn, 'name')}`,
          position: 'top',
          color: 'green',
          timeout: 3000
        })
      } catch (e) {
        this.$q.notify({
          message: this.$gettext('You could not be subscribed'),
          position: 'top',
          color: 'red',
          timeout: 3000
        })
        throw e
      }
    },
    async unsubscribe (addOn) {
      try {
        await this.$apollo.mutate({
          mutation: mutationDeleteAddOnSubscriptionByPk,
          variables: {
            id: addOn.add_on_subscriptions[0].id
          }
        })
        this.$q.notify({
          message: `${this.$gettext('You are now unsubscribed from')} ${getI18nField(addOn, 'name')}`,
          position: 'top',
          color: 'green'
        })
      } catch (e) {
        this.$q.notify({
          message: this.$gettext('You could not be unsubscribed'),
          position: 'top',
          color: 'red'
        })
        throw e
      }
    }
  }
}
