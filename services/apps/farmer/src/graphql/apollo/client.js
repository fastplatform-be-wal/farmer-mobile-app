import { ApolloClient, split, fromPromise, createHttpLink } from '@apollo/client/core'
import { onError } from '@apollo/client/link/error'
import { WebSocketLink } from '@apollo/client/link/ws'
import { setContext } from '@apollo/client/link/context'
import { getMainDefinition } from '@apollo/client/utilities'

import { cache } from './cache'
import store from 'src/store'
import { getRefreshedAuthToken } from 'src/services/authentication'
import { onNetworkError, onRefreshTokenError } from 'src/services/error-handler'

function getAuthorizationHeader () {
  const tokens = store.state.fastplatform.tokens

  if (tokens !== null && tokens.accessToken) {
    return 'Bearer ' + tokens.accessToken
  }
  return null
}

function getConnectionParams () {
  const params = {
    headers: {}
  }
  const authHeader = getAuthorizationHeader()
  if (authHeader) {
    params.headers.Authorization = authHeader
  }
  const userLanguage = store.state.fastplatform.preferredLanguage || 'en'
  params.headers['User-Language'] = userLanguage
  return params
}

const webSocketLink = new WebSocketLink({
  uri: null,
  options: {
    lazy: true,
    reconnect: true,
    inactivityTimeout: 100000, // timeout in ms before disconnecting from WS
    connectionParams: getConnectionParams
  }
})

const httpLink = createHttpLink({
  uri () {
    return store.state.fastplatform.config.apollo.fastplatform.httpLink.uri
  }
})

// function that returns an 'apollo client' instance
export default function createApolloClient () {
  function operationIsSubscription (operation) {
    const definition = getMainDefinition(operation.query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  }

  // create apollo client http link
  let link = split(
    operationIsSubscription,
    webSocketLink,
    httpLink
  )

  // set headers with token if present
  const setAuthorizationLink = setContext(async () => {
    return getConnectionParams()
  })

  // update WSS url dynamically
  const updateWebSocketUrl = setContext(() => {
    webSocketLink.subscriptionClient.url = store.state.fastplatform.config.apollo.fastplatform.wsLink.uri
  })

  link = updateWebSocketUrl.concat(setAuthorizationLink.concat(link))

  let isRefreshing = false
  let pendingRequests = []

  const resolvePendingRequests = () => {
    pendingRequests.map(callback => callback())
    pendingRequests = []
  }

  const errorHandler = ({ graphQLErrors, networkError, operation, forward }) => {
    if (graphQLErrors) {
      for (const err of graphQLErrors) {
        if (err?.extensions?.code) {
          switch (err.extensions.code) {
            case 'unexpected': {
              if (err.message === 'webhook authentication request failed') {
                console.error('API authentication failure: logout user?')
              }
              break
            }
            case 'access-denied': {
              let forward$
              if (!isRefreshing) {
                isRefreshing = true
                forward$ = fromPromise(
                  getRefreshedAuthToken()
                    .then(() => {
                      resolvePendingRequests()
                    })
                    .catch(() => {
                      pendingRequests = []
                      onRefreshTokenError()
                    })
                    .finally(() => {
                      isRefreshing = false
                    })
                )
              } else {
                forward$ = fromPromise(
                  new Promise(resolve => {
                    pendingRequests.push(() => resolve())
                  })
                )
              }
              return forward$.flatMap(() => forward(operation))
            }
          }
        }
      }
    }
    if (networkError) {
      onNetworkError()
    }
  }

  const onErrorlink = onError(errorHandler)

  link = onErrorlink.concat(link)

  // create an `apollo client` instance
  return new ApolloClient({
    link,
    cache,
    typeDefs: null,
    resolvers: null,
    defaultOptions: {
      watchQuery: {
        fetchPolicy: 'cache-and-network'
      },
      query: {
        fetchPolicy: 'network-only'
      }
    },
    connectToDevTools: process.env.DEV
  })
}
