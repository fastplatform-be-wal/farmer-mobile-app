export default [
  {
    path: '/fieldbook/fertilization/fertilization-plan/navigator-f3/crop-profile',
    name: 'navigatorF3CropProfile',
    component: () => import(/* webpackChunkName: "navigator-f3" */ 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/pages/NavigatorF3CropProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/navigator-f3/harvest-and-residues',
    name: 'navigatorF3HarvestAndResidues',
    component: () => import(/* webpackChunkName: "navigator-f3" */ 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/pages/NavigatorF3HarvestAndResiduesPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/navigator-f3/pk-strategy',
    name: 'navigatorF3PKStrategy',
    component: () => import(/* webpackChunkName: "navigator-f3" */ 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/pages/NavigatorF3PKStrategyPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/navigator-f3/soil',
    name: 'navigatorF3Soil',
    component: () => import(/* webpackChunkName: "navigator-f3" */ 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/pages/NavigatorF3SoilPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/navigator-f3/irrigation',
    name: 'navigatorF3Irrigation',
    component: () => import(/* webpackChunkName: "navigator-f3" */ 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/pages/NavigatorF3IrrigationPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/navigator-f3/nutrient-balance',
    name: 'navigatorF3NutrientBalance',
    component: () => import(/* webpackChunkName: "navigator-f3" */ 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/pages/NavigatorF3NutrientBalancePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/navigator-f3/young-trees',
    name: 'navigatorF3YoungTrees',
    component: () => import(/* webpackChunkName: "navigator-f3" */ 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/pages/NavigatorF3YoungTreesPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/navigator-f3/young-trees-fertilization',
    name: 'navigatorF3YoungTreesFertilization',
    component: () => import(/* webpackChunkName: "navigator-f3" */ 'src/pages/fieldbook/fertilization/algorithms/navigator-f3/pages/NavigatorF3YoungTreesFertilizationPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
