export default [
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ 'pages/login/oauth-login/OauthLoginPage'),
    meta: {
      isPublic: true
    }
  },
  {
    path: '/login/cb',
    name: 'loginCallback',
    component: () => import(/* webpackChunkName: "login" */ 'pages/login/oauth-login/OauthLoginCallbackPage'),
    meta: {
      isPublic: true
    }
  },
  {
    path: '/login/iacs/refresh',
    name: 'iacsRefreshCallback',
    component: () => import(/* webpackChunkName: "login" */ 'pages/login/iacs/IacsRefreshTokenPage'),
    meta: {
      isPublic: true
    }
  },
  {
    path: '/region-selection',
    name: 'regionSelection',
    component: () => import(/* webpackChunkName: "region-selection" */ 'pages/login/region-selection/RegionSelectionPage'),
    meta: {
      isPublic: true
    }
  }
]
