export default [
  // {
  //   path: '/fieldbook/fertilization/fertilization-plan/visione/plot',
  //   name: 'visionePlot',
  //   component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/VisionePlotPage'),
  //   meta: {
  //     footerGroupName: 'fieldbook'
  //   }
  // },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/crop-profile-pre-selection',
    name: 'visioneCropSelection',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/VisioneCropSelectionPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/tree-crop-production-phase',
    name: 'visioneTreeCropProductionPhase',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/tree-crop-pages/VisioneTreeCropProductionPhasePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/tree-fertilizers-selection',
    name: 'visioneTreeFertilizationsSelectionPage',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/tree-crop-pages/VisioneTreeFertilizationsSelectionPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/crop-rotations',
    name: 'visioneCropRotations',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/rotation/VisioneCropRotationsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/crop-rotations/new-rotation/crop-profile',
    name: 'visioneAddCropRotationProfile',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/rotation/VisioneAddCropRotationProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/crop-rotations/new-rotation/cycles-and-yield',
    name: 'visioneAddCropRotationCyclesAndYield',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/rotation/VisioneAddCropRotationCyclesAndYieldPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/crop-rotations/new-rotation/residues',
    name: 'visioneAddCropRotationResidues',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/rotation/VisioneAddCropRotationResiduesPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/crop-rotations/new-rotation/fertilizations',
    name: 'visioneAddCropRotationFertilizations',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/rotation/VisioneAddCropRotationFertilizationsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/previous-campaign/profile',
    name: 'visionePreviousCampaignProfile',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/previous-campaign/VisioneProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/previous-campaign/residues',
    name: 'visionePreviousCampaignResidues',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/previous-campaign/VisioneResiduesPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/previous-campaign/organic-fertilization',
    name: 'visionePreviousCampaignOrganicFertilization',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/previous-campaign/VisioneOrganicFertilizationPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/soil/characteristics',
    name: 'visioneSoilCharacteristics',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/soil/VisioneSoilPage'),
    meta: {
      footerGroupName: 'fieldbook'
    },
    props: { section: 'characteristics' }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/soil/samples',
    name: 'visioneSoilSamples',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/non-tree-crop-pages/soil/VisioneSoilPage'),
    meta: {
      footerGroupName: 'fieldbook'
    },
    props: { section: 'samples' }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/compute',
    name: 'visioneCompute',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/VisioneComputePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/visione/results',
    name: 'visioneResults',
    component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/pages/VisioneResultsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
  // {
  //   path: '/fieldbook/fertilization/fertilization-plan/visione/soil',
  //   name: 'visioneSoil',
  //   component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/VisioneSoilPage'),
  //   meta: {
  //     footerGroupName: 'fieldbook'
  //   }
  // },
  // {
  //   path: '/fieldbook/fertilization/fertilization-plan/visione/nitrogen',
  //   name: 'visioneNitrogen',
  //   component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/VisioneNitrogenPage'),
  //   meta: {
  //     footerGroupName: 'fieldbook'
  //   }
  // },
  // {
  //   path: '/fieldbook/fertilization/fertilization-plan/visione/strategy',
  //   name: 'visioneStrategy',
  //   component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/VisioneStrategyPage'),
  //   meta: {
  //     footerGroupName: 'fieldbook'
  //   }
  // },
  // {
  //   path: '/fieldbook/fertilization/fertilization-plan/visione/compute',
  //   name: 'visioneCompute',
  //   component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/VisioneComputePage'),
  //   meta: {
  //     footerGroupName: 'fieldbook'
  //   }
  // },
  // {
  //   path: '/fieldbook/fertilization/fertilization-plan/visione/npk-results',
  //   name: 'visioneNPKResults',
  //   component: () => import(/* webpackChunkName: "visione" */ 'src/pages/fieldbook/fertilization/algorithms/visione/VisioneNPKResultsPage'),
  //   meta: {
  //     footerGroupName: 'fieldbook'
  //   }
  // }
]
