export default [
  {
    path: '/fieldbook/fertilization/fertilization-plan/arc/crop-selection',
    name: 'arcCropSelection',
    component: () => import(/* webpackChunkName: "arc" */ 'src/pages/fieldbook/fertilization/algorithms/arc/pages/ArcCropSelectionPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/arc/results',
    name: 'arcResults',
    component: () => import(/* webpackChunkName: "arc" */ 'src/pages/fieldbook/fertilization/algorithms/arc/pages/ArcResultsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
