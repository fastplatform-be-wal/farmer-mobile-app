export default [
  {
    path: '/fieldbook',
    name: 'fieldbook',
    component: () => import(/* webpackChunkName: "fieldbook" */ 'pages/fieldbook/FieldbookPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
