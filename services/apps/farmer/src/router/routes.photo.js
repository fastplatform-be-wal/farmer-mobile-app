import featureFlags from 'src/services/feature-flags'

export default [
  {
    path: '/photos',
    name: 'photos',
    component: () => import(/* webpackChunkName: "photo" */ 'pages/photos/photos/PhotosPage'),
    beforeEnter: (to, from, next) => {
      if (featureFlags.canDisplayGeotaggedPhoto()) {
        next()
      } else {
        next(false)
      }
    },
    meta: {
      footerGroupName: 'photos'
    }
  },
  {
    path: '/photos/local/:fileName',
    name: 'photoLocal',
    component: () => import(/* webpackChunkName: "photo" */ 'pages/photos/photo/PhotoPage'),
    beforeEnter: (to, from, next) => {
      if (featureFlags.canDisplayGeotaggedPhoto()) {
        next()
      } else {
        next(false)
      }
    },
    props: (route) => ({
      mode: 'local',
      fileName: route.params.fileName
    }),
    meta: {
      footerGroupName: 'photos'
    }
  },
  {
    path: '/photos/:idPhoto',
    name: 'photo',
    component: () => import(/* webpackChunkName: "photo" */ 'pages/photos/photo/PhotoPage'),
    beforeEnter: (to, from, next) => {
      if (featureFlags.canDisplayGeotaggedPhoto()) {
        next()
      } else {
        next(false)
      }
    },
    props: (route) => ({
      mode: 'live',
      id: route.params.idPhoto
    }),
    meta: {
      footerGroupName: 'photos'
    }
  },
  {
    path: '/take-photo/:plotId?',
    name: 'takePhoto',
    component: () => import(/* webpackChunkName: "photo" */ 'pages/photos/take-photo/TakePhotoPage'),
    beforeEnter: (to, from, next) => {
      if (featureFlags.canTakePhoto()) {
        next()
      } else {
        next(false)
      }
    },
    props: true,
    meta: {
      footerGroupName: 'photos'
    }
  },
  {
    path: '/take-photo/preview',
    name: 'photoPreview',
    component: () => import(/* webpackChunkName: "photo" */ 'pages/photos/photo/PhotoPage'),
    beforeEnter: (to, from, next) => {
      if (featureFlags.canTakePhoto()) {
        next()
      } else {
        next(false)
      }
    },
    props: true,
    meta: {
      footerGroupName: 'photos'
    }
  }

]
