export default [
  // {
  //   path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/plot',
  //   name: 'fertilicalcPlot',
  //   component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/FertilicalcPlotPage'),
  //   meta: {
  //     footerGroupName: 'fieldbook'
  //   }
  // },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/crop-profile',
    name: 'fertilicalcCropProfile',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcCropProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/irrigation-yield',
    name: 'fertilicalcIrrigationYield',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcIrrigationYieldPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/previous-campaign',
    name: 'fertilicalcPreviousCampaign',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcPreviousCampaignPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/previous-campaign/residues',
    name: 'fertilicalcPreviousCampaignResidues',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcPreviousCampaignResiduesPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/soil',
    name: 'fertilicalcSoil',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcSoilPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/nitrogen',
    name: 'fertilicalcNitrogen',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcNitrogenPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/strategy',
    name: 'fertilicalcStrategy',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcStrategyPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/compute',
    name: 'fertilicalcCompute',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcComputePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/npk-results',
    name: 'fertilicalcNPKResults',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/npk-recommendation/FertilicalcNPKResultsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/n-share',
    name: 'fertilicalcNShare',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/fertilization-strategy/FertilicalcNSharePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/manure',
    name: 'fertilicalcManure',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/fertilization-strategy/FertilicalcManurePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/basal',
    name: 'fertilicalcBasal',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/fertilization-strategy/FertilicalcBasalPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/fertilicalc/topdressing',
    name: 'fertilicalcTopdressing',
    component: () => import(/* webpackChunkName: "fertilicalc" */ 'src/pages/fieldbook/fertilization/algorithms/fertilicalc/pages/fertilization-strategy/FertilicalcTopdressingPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
