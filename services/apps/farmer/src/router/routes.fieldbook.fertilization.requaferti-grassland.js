export default [
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-grassland/crop-profile',
    name: 'requafertiGrasslandCropProfile',
    component: () => import(/* webpackChunkName: "requaferti-grassland" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-grassland/pages/RequafertGrasslandCropProfilePage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-grassland/soil-profile',
    name: 'requafertiGrasslandSoilPage',
    component: () => import(/* webpackChunkName: "requaferti-grassland" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-grassland/pages/RequafertiGrasslandSoilPage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-grassland/clover-coverage',
    name: 'requafertiGrasslandLeguminousPage',
    component: () => import(/* webpackChunkName: "requaferti-grassland" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-grassland/pages/RequafertiGrasslandLeguminousEffectPage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-grassland/performace-objectives',
    name: 'requafertiGrasslandYieldGoalPage',
    component: () => import(/* webpackChunkName: "requaferti-grassland" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-grassland/pages/RequafertiGrasslandYieldGoalPage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-grassland/organic-manure',
    name: 'requafertiGrasslandOrganicManurePage',
    component: () => import(/* webpackChunkName: "requaferti-grassland" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-grassland/pages/RequafertiGrasslandOrganicManurePage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-grassland/compute',
    name: 'requafertiGrasslandCompute',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-grassland/pages/RequafertiGrasslandComputePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-grassland/results',
    name: 'requafertiGrasslandResults',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-grassland/pages/RequafertiGrasslandResultsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-grassland/fertilizer',
    name: 'requafertiGrasslandFertilizerDistributionPage',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-grassland/pages/RequafertiGrasslandFertilizerDistributionPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
