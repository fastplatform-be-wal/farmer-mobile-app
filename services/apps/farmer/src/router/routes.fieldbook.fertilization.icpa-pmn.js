export default [
  {
    path: '/fieldbook/fertilization/fertilization-plan/icpa-pmn/crop-profile',
    name: 'icpaPmnCropProfile',
    component: () => import(/* webpackChunkName: "icpa-pmn" */ 'src/pages/fieldbook/fertilization/algorithms/icpa-pmn/pages/IcpaPmnCropProfilePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/icpa-pmn/harvest-and-residues',
    name: 'icpaPmnHarvestAndResidues',
    component: () => import(/* webpackChunkName: "icpa-pmn" */ 'src/pages/fieldbook/fertilization/algorithms/icpa-pmn/pages/IcpaPmnHarvestAndResiduesPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/icpa-pmn/soil',
    name: 'icpaPmnSoil',
    component: () => import(/* webpackChunkName: "icpa-pmn" */ 'src/pages/fieldbook/fertilization/algorithms/icpa-pmn/pages/IcpaPmnSoilPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/icpa-pmn/livestock',
    name: 'icpaPmnLivestock',
    component: () => import(/* webpackChunkName: "icpa-pmn" */ 'src/pages/fieldbook/fertilization/algorithms/icpa-pmn/pages/IcpaPmnLivestockPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/icpa-pmn/manure',
    name: 'icpaPmnManure',
    component: () => import(/* webpackChunkName: "icpa-pmn" */ 'src/pages/fieldbook/fertilization/algorithms/icpa-pmn/pages/IcpaPmnManurePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/icpa-pmn/mineral-fertilization',
    name: 'icpaPmnMineralFertilization',
    component: () => import(/* webpackChunkName: "icpa-pmn" */ 'src/pages/fieldbook/fertilization/algorithms/icpa-pmn/pages/IcpaPmnMineralFertilizationPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/icpa-pmn/compute-plan',
    name: 'icpaPmnMineralComputePlan',
    component: () => import(/* webpackChunkName: "icpa-pmn" */ 'src/pages/fieldbook/fertilization/algorithms/icpa-pmn/pages/IcpaPmnComputePlanPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
