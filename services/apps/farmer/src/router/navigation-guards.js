// Define the router navigation guards here
import store from 'src/store'

export async function beforeEach (to, from, next) {
  if (to.name === 'logout') {
    // Logout route: always allowed
    next()
  } else {
    const isPublicRoute = Boolean(to.meta?.isPublic)
    const isConfigInvalid = store.getters['fastplatform/isConfigInvalid']
    const isAvailableWithoutHoldingCampaign = Boolean(to.meta?.isAvailableWithoutHoldingCampaign)
    const isActiveHoldingCampaignValid = store.getters['fastplatform/isActiveHoldingCampaignValid']
    if (isPublicRoute) {
      // The user accesses a public route
      if (isConfigInvalid) {
        // The user is not logged in and accesses a public page
        next()
      } else {
        // The user is logged in but tried to access a public page: redirect him to the home page
        next({ name: 'home' })
      }
    } else if (isConfigInvalid) {
      // The user has some missing login information and tried to access a restricted page: reset the config then to go the region selection page
      console.warn('[NavigationGuard] Invalid config: logging out user', to)
      next({ name: 'logout' })
    } else if (!isAvailableWithoutHoldingCampaign && !isActiveHoldingCampaignValid) {
      // The user is logged in but has no valid active holding campaign: redirect him to the holdings page
      next({ name: 'holdings' })
    } else {
      // Otherwise just continue the navigation to a restricted route
      next()
    }
  }
}

export function afterEach (to, from) {
  //
}
