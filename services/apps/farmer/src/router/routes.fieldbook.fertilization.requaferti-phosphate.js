export default [
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-phosphate/crop-profile',
    name: 'requafertiPhosphateCropProfile',
    component: () => import(/* webpackChunkName: "requaferti-phosphate" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-phosphate/pages/RequafertiPhosphateCropProfilePage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-phosphate/soil-profile',
    name: 'requafertiPhosphateSoilPage',
    component: () => import(/* webpackChunkName: "requaferti-phosphate" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-phosphate/pages/RequafertiPhosphateSoilPage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-phosphate/previous-campaign',
    name: 'requafertiPhosphatePreviousCampaignPage',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-phosphate/pages/RequafertiPhosphatePreviousCampaignPage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-phosphate/organic-manure',
    name: 'requafertiPhosphateOrganicManurePage',
    component: () => import(/* webpackChunkName: "requaferti-phosphate" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-phosphate/pages/RequafertiPhosphateOrganicManurePage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-phosphate/mineral-fertilization',
    name: 'requafertiPhosphateMineralFertilizationPage',
    component: () => import(/* webpackChunkName: "requaferti-phosphate" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-phosphate/pages/RequafertiPhosphateMineralFertilizationPage.vue'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-phosphate/compute',
    name: 'requafertiPhosphateCompute',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-phosphate/pages/RequafertiPhosphateComputePage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  },
  {
    path: '/fieldbook/fertilization/fertilization-plan/requaferti-phosphate/results',
    name: 'requafertiPhosphateResults',
    component: () => import(/* webpackChunkName: "requaferti" */ 'src/pages/fieldbook/fertilization/algorithms/requaferti-phosphate/pages/RequafertiPhosphateResultsPage'),
    meta: {
      footerGroupName: 'fieldbook'
    }
  }
]
