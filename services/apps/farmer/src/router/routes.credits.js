export default [
  {
    path: '/credits',
    name: 'credits',
    component: () => import(/* webpackChunkName: "credits" */ 'pages/credits/CreditsPage'),
    meta: {
      isAvailableWithoutHoldingCampaign: true
    }
  }
]
