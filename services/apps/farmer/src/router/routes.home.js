export default [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ 'pages/home/HomePage'),
    meta: {
      footerGroupName: 'home'
    }
  }
]
