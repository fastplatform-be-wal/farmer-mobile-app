export const initialState = () => {
  return {
    target: {},
    parameters: {},
    results: {},
    fertilizationPlanIdAndName: {
      id: null,
      name: null
    },
    algorithm: {},
    isEdition: false,
    isCreationFromPlot: false
  }
}
export default function () {
  return initialState()
}
