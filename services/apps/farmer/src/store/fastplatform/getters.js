export function isConfigInvalid (state) {
  return (
    (!state.config || Object.keys(state.config).length === 0) ||
    (!isOpenIDConfigValid(state)) ||
    (!state.user || Object.keys(state.user).length === 0) ||
    (!state.tokens || Object.keys(state.tokens).length === 0)
  )
}

export function isRefreshTokenConfigValid (state) {
  const loginConfig = state.config?.login
  const hasClientId = Boolean(loginConfig?.clientId)
  const hasRefreshUrl = Boolean(loginConfig?.token_endpoint)
  const hasRefreshToken = Boolean(state.tokens?.refreshToken)
  return hasClientId && hasRefreshUrl && hasRefreshToken
}

export function isOpenIDConfigValid (state) {
  const loginConfig = state.config?.login
  const hasAuthorizationEndpoint = Boolean(loginConfig?.authorization_endpoint)
  const hasTokenEndpoint = Boolean(loginConfig?.token_endpoint)
  const hasUserInfoEndpoint = Boolean(loginConfig?.userinfo_endpoint)
  return hasAuthorizationEndpoint && hasTokenEndpoint && hasUserInfoEndpoint
}

export function isActiveHoldingCampaignValid (state) {
  const holdingCampaign = state.holdingCampaign
  return holdingCampaign?.id != null &&
    holdingCampaign.campaign?.id != null &&
    holdingCampaign.campaign.name &&
    holdingCampaign.holding?.id != null &&
    holdingCampaign.holding.name
}
