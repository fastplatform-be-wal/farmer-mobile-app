import { canChangePlatform } from 'src/utils/device'
import { initialState } from './state'

export function changeUserPreferredPlatform (context, value) {
  return new Promise((resolve, reject) => {
    // Verify that the Platform could be changed
    if (canChangePlatform()) {
      value = value.toLowerCase()
      if (['mobile', 'desktop'].includes(value)) {
        // Change the user preference
        context.commit('changeUserPreferredPlatform', value)
        // Change the Platform according to the user preference
        context.commit('changePlatform', value)
        resolve()
      } else {
        reject('Wrong Platform type. Possible value are: "mobile" and "desktop".')
      }
    } else {
      reject('The Platform cannot be changed for the device on platform')
    }
  })
}

export function showLeftDrawer (context) {
  context.commit('updateIsLeftDrawerOpened', true)
}

export function hideLeftDrawer (context) {
  context.commit('updateIsLeftDrawerOpened', false)
}

export function toggleLeftDrawer (context) {
  context.commit('updateIsLeftDrawerOpened', !context.state.isLeftDrawerOpened)
}

export function reset (context) {
  context.commit('reset', initialState())
}
