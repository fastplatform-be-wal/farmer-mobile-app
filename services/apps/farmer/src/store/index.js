import { createStore } from 'vuex'
import VuexPersistence from 'vuex-persist'

import appState from './app-state'
import fastplatform from './fastplatform'
import fertilizationPlan from './fertilization-plan'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

const vuexPersistence = new VuexPersistence({
  storage: window.localStorage
})

const Store = createStore({
  modules: {
    fastplatform,
    fertilizationPlan,
    appState
  },
  plugins: [vuexPersistence.plugin],
  actions: {
    reset: (context) => {
      return new Promise((resolve, reject) => {
        const promises = [
          context.dispatch('fastplatform/reset'),
          context.dispatch('fertilizationPlan/reset'),
          context.dispatch('appState/reset')
        ]
        Promise.all(promises).then(() => {
          if (!context.getters['fastplatform/isConfigInvalid']) {
            // seems like the reset didn't actually work. Clear the localstorage
            console.error('Store was not properly resetted. Proceeding to hard cleaning ...')
            window.localStorage.clear()
            window.sessionStorage.clear()
          }
          resolve()
        }).catch((e) => {
          console.error("Couldn't reset store. Proceeding to hard cleaning ...")
          window.localStorage.clear()
          window.sessionStorage.clear()
          resolve()
        })
      })
    }
  },
  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV
})

export default Store
