// TODO implement onupgrade needed (IDBVersionChangeEvent error)
export function indexedDBHardClear (apolloClient, retry = 0) {
  return new Promise((resolve, reject) => {
    const req = window.indexedDB.deleteDatabase('localforage')
    req.onsuccess = function () {
      resolve()
    }
    req.onerror = function (e) {
      if (retry === 1) {
        // try to stop apollo client
        if (apolloClient) {
          apolloClient.stop()
        }
        // Now retry
        indexedDBHardClear(apolloClient, retry - 1).then(() => resolve()).catch((e) => reject(e))
      } else {
        console.error('Failed to hard clear indexedDB localforage database', e)
        reject(e)
      }
    }
    req.onblocked = function (e) {
      if (retry === 1) {
        if (apolloClient) {
          // try to stop apollo client
          apolloClient.stop()
        }
        // Now retry
        indexedDBHardClear(apolloClient, retry - 1).then(() => resolve()).catch((e) => reject(e))
      } else {
        console.error('Failed to hard clear indexedDB localforage database', e)
        reject(e)
      }
    }
  })
}
