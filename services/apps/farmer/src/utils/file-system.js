import { Filesystem, Directory } from '@capacitor/filesystem'

/**
 * Functions from Capacitor FileSystem plugin to work with files on the device:
 * https://capacitor.ionicframework.com/docs/apis/filesystem
 * FileSystem interfaces:
 * https://capacitor.ionicframework.com/docs/apis/filesystem#interfaces
 */

/**
 * Async file write
 * @param {FileWriteOptions} fileWriteOptions - File write options
 * @returns {Promise<({uri: string}|{error: string})>} A dict containing the uri of the file
 */
export async function fileWrite (fileWriteOptions) {
  try {
    return await Filesystem.writeFile(fileWriteOptions)
  } catch (e) {
    return { error: e }
  }
}

/**
 * Sync file write
 * @param {FileWriteOptions} fileWriteOptions - File write options
 * @returns {Promise<{uri: string}>} A promise that resolves with the file write result or an error
 */
export function fileWriteSync (fileWriteOptions) {
  return Filesystem.writeFile(fileWriteOptions)
}

export async function fileRead (options) {
  return await Filesystem.readFile(options)
}

// async function fileAppend() {
//   await Filesystem.appendFile({
//     path: 'secrets/text.txt',
//     data: "MORE TESTS",
//     directory: FilesystemDirectory.Documents,
//     encoding: FilesystemEncoding.UTF8
//   });
// }

/**
 * Delete a file from the device.
 * @param {String} filePath File path inside the File system directory
 * @param {FilesystemDirectory} FSDirectory File system directory: https://capacitor.ionicframework.com/docs/apis/filesystem#type-432
 * Default to FilesystemDirectory.Data
 * @returns {Promise<{}>} Empty promise
 */
export async function fileDelete (filePath, FSDirectory = Directory.Data) {
  return await Filesystem.deleteFile({
    path: filePath,
    directory: FSDirectory
  })
}

/**
 * Async - Create a directory or a path of directories
 * @param {MkdirOptions} mkdirOptions - mkdir options
 * @returns {({} | {error: string})} An empty dict if successful an dict containing an error otherwise
 */
export async function mkdir (mkdirOptions) {
  try {
    return await Filesystem.mkdir(mkdirOptions)
  } catch (e) {
    return { error: e }
  }
}

export async function rmdir (options) {
  try {
    return await Filesystem.rmdir(options)
  } catch (e) {
    return { error: e }
  }
}

export async function readdir (options) {
  try {
    return await Filesystem.readdir(options)
  } catch (e) {
    return { error: e }
  }
}

// async function stat() {
//   try {
//     let ret = await Filesystem.stat({
//       path: 'secrets/text.txt',
//       directory: FilesystemDirectory.Documents
//     });
//   } catch (e) {
//     console.error('Unable to stat file', e);
//   }
// }

// async function readFilePath() {
//   // Here's an example of reading a file with a full file path. Use this to
//   // read binary data (base64 encoded) from plugins that return File URIs, such as
//   // the Camera.
//   try {
//     let data = await Filesystem.readFile({
//       path: 'file:///var/mobile/Containers/Data/Application/22A433FD-D82D-4989-8BE6-9FC49DEA20BB/Documents/text.txt'
//     })
//   }
// }

// async function rename() {
//   try {
//     // This example moves the file within the same 'directory'
//     let ret = await Filesystem.rename({
//       from: 'text.txt',
//       to: 'text2.txt',
//       directory: FilesystemDirectory.Documents
//     });
//   } catch (e) {
//     console.error('Unable to rename file', e);
//   }
// }

// async function copy() {
//   try {
//     // This example copies a file within the documents directory
//     let ret = await Filesystem.copy({
//       from: 'text.txt',
//       to: 'text2.txt',
//       directory: FilesystemDirectory.Documents
//     });
//   } catch (e) {
//     console.error('Unable to copy file', e);
//   }
// }

export async function getFileUri (options) {
  try {
    return await Filesystem.getUri(options)
  } catch (e) {
    return { error: e }
  }
}
