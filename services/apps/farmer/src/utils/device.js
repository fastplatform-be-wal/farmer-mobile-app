import { Platform } from 'quasar'

const desktopPlatform = 'desktop'
const mobilePlatform = 'mobile'

function canChangePlatform () {
  // Can change the platform 'layout' if the app is open on a browser on a mobile device.
  return process.env.DEV || (!Platform.is.capacitor && !Platform.is.desktop && Platform.is.mobile)
}

function platformToDisplay (userPreference) {
  function getUserPreference (userPreference) {
    if (userPreference) {
      return userPreference.toLowerCase() === 'desktop' ? desktopPlatform : mobilePlatform
    } else {
      return Platform.is.mobile ? mobilePlatform : desktopPlatform
    }
  }
  // Load the Platform depending on the platform
  // and the user preference if available
  if (process.env.DEV) {
    // special case for the dev environment. The user preference has the upper hand over device.
    return getUserPreference(userPreference)
  } else if (Platform.is.capacitor) {
    // Android or iOS app. Only mobile Platform
    return mobilePlatform
  } else if (Platform.is.mobile) {
    // The user is on a mobile device (smartphone or tablet and access the website using a browser)
    // Both Platform are valid. Default to mobile.
    return getUserPreference(userPreference)
  } else if (Platform.is.desktop) {
    // Desktop Platform only.
    return desktopPlatform
  } else {
    // By default display the desktop Platform
    return desktopPlatform
  }
}

export { platformToDisplay, canChangePlatform }
