import { apolloClient } from 'src/boot/apollo'
import store from 'src/store'
import queryHoldingCampaignCentroidsSample from 'src/services/graphql/queryHoldingCampaignCentroidsSample'

const PLOT_LIMIT_DEFAULT = 20

export const updateHoldingCampaignCentroidAndBounds = async (plotsLimit = PLOT_LIMIT_DEFAULT) => {
  const holdingCampaign = {
    ...store.state.fastplatform.holdingCampaign
  }
  try {
    const { data } = await apolloClient.query({
      query: queryHoldingCampaignCentroidsSample,
      variables: {
        holding_campaign_id: holdingCampaign.id,
        plots_limit: plotsLimit
      }
    })
    const plots = data?.plot?.filter(plot => plot.centroid)
    if (!plots?.length) {
      throw new Error('Holding campaign has no plots with centroid.')
    } else {
      holdingCampaign.bounds = plots.map(plot => [plot.centroid.coordinates[1], plot.centroid.coordinates[0]])
      const sum = (accumulator, currentValue) => accumulator + currentValue
      holdingCampaign.centroid = {
        type: 'Point',
        coordinates: [
          plots.map(plot => plot.centroid.coordinates[0]).reduce(sum) / plots.length,
          plots.map(plot => plot.centroid.coordinates[1]).reduce(sum) / plots.length
        ]
      }
    }
  } catch (e) {
    // set portal map defaults for holding campaign
    const mapDefaults = store.state.fastplatform.portalConfiguration.mapDefaults
    if (mapDefaults) {
      console.warn('Could not compute holding campaign centroid and bounds: applying portal map defaults')
      holdingCampaign.centroid = {
        type: 'Point',
        coordinates: [
          mapDefaults.centerLongitude,
          mapDefaults.centerLatitude
        ]
      }
      // bounds are not available (but could be computed with zoom level?)
    } else {
      console.error('Could not update holding campaign centroid and bounds')
    }
  }
  store.commit('fastplatform/updateHoldingCampaign', holdingCampaign)
}
