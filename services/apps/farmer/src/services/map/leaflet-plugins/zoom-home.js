/*
 * Leaflet zoom control with a home button for resetting the view.
 *
 * Distributed under the CC-BY-SA-3.0 license. See the file "LICENSE"
 * for details.
 *
 * Based on code by toms (https://gis.stackexchange.com/a/127383/48264).
 */
import * as L from 'leaflet'

L.Control.ZoomHome = L.Control.Zoom.extend({
  options: {
    position: 'topleft',
    zoomHomeTitle: 'Home'
  },

  onAdd: function (map) {
    const container = L.DomUtil.create('div', 'leaflet-control-zoomhome leaflet-bar')
    const options = this.options

    options.homeCoordinates = map.getCenter()
    options.homeZoom = map.getZoom()

    this._zoomHomeButton = this._createButton(
      '<i class="fa fa-home" style="line-height:1.65;"></i>',
      options.zoomHomeTitle,
      'leaflet-control-zoomhome-home',
      container,
      this._zoomHome.bind(this)
    )

    return container
  },

  setHomeBounds: function (bounds) {
    if (bounds === undefined) {
      bounds = this._map.getBounds()
    } else {
      if (typeof bounds.getCenter !== 'function') {
        bounds = L.latLngBounds(bounds)
      }
    }
    this.options.homeZoom = this._map.getBoundsZoom(bounds)
    this.options.homeCoordinates = bounds.getCenter()
  },

  setHomeCoordinates: function (coordinates) {
    if (coordinates === undefined) {
      coordinates = this._map.getCenter()
    }
    this.options.homeCoordinates = coordinates
  },

  setHomeZoom: function (zoom) {
    if (zoom === undefined) {
      zoom = this._map.getZoom()
    }
    this.options.homeZoom = zoom
  },

  getHomeZoom: function () {
    return this.options.homeZoom
  },

  getHomeCoordinates: function () {
    return this.options.homeCoordinates
  },

  _zoomHome: function (e) {
    this._map.setView(this.options.homeCoordinates, this.options.homeZoom)
  }
})

L.Control.zoomHome = function (options) {
  return new L.Control.ZoomHome(options)
}
