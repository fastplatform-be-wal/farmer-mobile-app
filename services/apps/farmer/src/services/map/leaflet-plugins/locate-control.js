import * as L from 'leaflet'
import 'leaflet.locatecontrol'
import { Geolocation } from '@capacitor/geolocation'

// Customize the leaflet locate control package to use the mobile geolocation features if available.
// https://github.com/domoritz/leaflet-locatecontrol
L.Control.MobileLocate = L.Control.Locate.extend({
  _activate: function () {
    if (!this._active) {
      this._active = true

      // start geolocation tracking
      //! FIX_MIGRATION Egnss4CapGeolocation disabled
      //! this call to Geolocation is used to detect permission denied errors because the Egnss4CapGeolocation does not always return these errors
      /* Geolocation.getCurrentPosition().catch(error => {
        if (error?.code === error.PERMISSION_DENIED) {
          this._onLocationPermissionDenied()
        }
      }) */
      // this._geolocationPlugin = this.options.locateControlUseGnssPosition ? Egnss4CapGeolocation : Geolocation

      this._geolocationPlugin = Geolocation
      const positionOptions = { enableHighAccuracy: this.options.locateOptions.enableHighAccuracy }
      this._geolocationPlugin.watchPosition(positionOptions, (position, error) => {
        if (error) {
          console.log('Geolocation error', error)
          if (error.code === error.PERMISSION_DENIED) {
            this._onLocationPermissionDenied()
          } else {
            this._onLocationError(this._convertToErrorEventStandard(error))
          }
        } else {
          this._customOnLocationFound(this._convertToLocationEventStandard(position), position)
        }
      }).then(watchId => {
        this._watchId = watchId
      }).catch(e => {
        console.error('Geolocation failed', e)
      })

      // keep the default events except for the location related ones
      // bind event listeners
      this._map.on('dragstart', this._onDrag, this)
      this._map.on('zoomstart', this._onZoom, this)
      this._map.on('zoomend', this._onZoomEnd, this)
      if (this.options.showCompass) {
        if (this.options.useNativeDeviceOrientation) {
          // use the native compass implementation via cordova-device-orientation-plugin: it doesn't require a permission on Safari/iOS but might be unreliable on some Android
          this._compassWatchId = navigator.compass.watchHeading(this._onCompassSuccess.bind(this), this._onCompassError.bind(this), { frequency: 100 })
        } else {
          // use the W3C DeviceOrientationEvent Spec (browser event)
          if ('ondeviceorientationabsolute' in window) {
            L.DomEvent.on(window, 'deviceorientationabsolute', this._onDeviceOrientation, this)
          } else if ('ondeviceorientation' in window) {
            L.DomEvent.on(window, 'deviceorientation', this._onDeviceOrientation, this)
          }
          // listen for device orientation change (deprecated spec!)
          if ('onorientationchange' in window) {
            L.DomEvent.on(window, 'orientationchange', this._onDeviceScreenOrientationChange, this)
          }
        }
      }
    }
    // else do nothing
  },

  _deactivate: function () {
    // Stop the geolocation watch
    if (this._watchId) {
      try {
        this._geolocationPlugin.clearWatch({ id: this._watchId })
      } catch (e) {
        console.error('[Leaflet-Locate-Control] Failed to stop geolocation watcher with id', this._watchId)
      }
    }
    this._active = false
    // default _deactivate operations
    if (!this.options.cacheLocation) {
      this._event = undefined
    }

    // unbind event listeners
    this._map.off('dragstart', this._onDrag, this)
    this._map.off('zoomstart', this._onZoom, this)
    this._map.off('zoomend', this._onZoomEnd, this)
    if (this.options.showCompass) {
      this._compassHeading = null
      if ('ondeviceorientationabsolute' in window) {
        L.DomEvent.off(window, 'deviceorientationabsolute', this._onDeviceOrientation, this)
      } else if ('ondeviceorientation' in window) {
        L.DomEvent.off(window, 'deviceorientation', this._onDeviceOrientation, this)
      }
      if ('onorientationchange' in window) {
        L.DomEvent.off(window, 'orientationchange', this._onDeviceScreenOrientationChange, this)
      }
    }
    if (this._compassWatchId) {
      navigator.compass.clearWatch(this._compassWatchId)
    }
  },

  _customOnLocationFound (positionFormatted, rawPosition) {
    // default locate control _onLocationFound function
    this._onLocationFound(positionFormatted)
    // custom onLocationFound function
    if (this.options.onLocationFound) {
      this.options.onLocationFound(rawPosition)
    }
  },

  _convertToLocationEventStandard (geolocationResult) {
    // Convert the gelocation result from capacitor or Egnss4CapGeolocation to a Leaflet LocationEvent object:
    // https://leafletjs.com/reference-1.6.0.html#locationevent
    let latlng
    if (geolocationResult.latestGnssScanCoords && geolocationResult.latestGnssScanCoords.latitude && geolocationResult.latestGnssScanCoords.longitude) {
      // use stabilized position from Egnss4CapGeolocation
      latlng = L.latLng(geolocationResult.latestGnssScanCoords.latitude, geolocationResult.latestGnssScanCoords.longitude)
      let altitude, altitudeAccuracy
      // if available use the altitude of the latest Egnss4CapGeolocation cluster point otherwise use the altitude
      // provided by android gms
      if (
        geolocationResult.latestGnssScanCoords.clusterPoints != null &&
        geolocationResult.latestGnssScanCoords.clusterPoints &&
        geolocationResult.latestGnssScanCoords.clusterPoints.length &&
        geolocationResult.latestGnssScanCoords.clusterPoints[geolocationResult.latestGnssScanCoords.clusterPoints.length - 1].altitude != null
      ) {
        altitude = geolocationResult.latestGnssScanCoords.clusterPoints[geolocationResult.latestGnssScanCoords.clusterPoints.length - 1].altitude
        altitudeAccuracy = undefined
      } else {
        altitude = geolocationResult.coords.altitude
        altitudeAccuracy = geolocationResult.coords.altitudeAccuracy || undefined
      }
      return {
        // latitude & longitude params are required for the locatecontrol plugin to properly set the view (bounds)
        latitude: geolocationResult.coords.latitude,
        longitude: geolocationResult.coords.longitude,
        latlng,
        bounds: geolocationResult.latestGnssScanCoords.precision ? latlng.toBounds(geolocationResult.latestGnssScanCoords.precision * 2) : latlng.toBounds(1), // set to 1 if precision is 0 or null
        // TODO set the GNSS precision as accuracy when it becomes usable
        // accuracy: geolocationResult.latestGnssScanCoords.precision,
        accuracy: geolocationResult.coords.accuracy,
        altitude,
        altitudeAccuracy,
        heading: geolocationResult.coords.heading, // the gnss data from Egnss4CapGeolocation does not have heading.
        speed: geolocationResult.coords.speed,
        timestamp: geolocationResult.latestGnssScanCoords.timestamp
      }
    } else {
      // use position from capacitor services (android gms)
      latlng = L.latLng(geolocationResult.coords.latitude, geolocationResult.coords.longitude)
      return {
        // latitude & longitude params are required for the locatecontrol plugin to properly set the view (bounds)
        latitude: geolocationResult.coords.latitude,
        longitude: geolocationResult.coords.longitude,
        latlng,
        bounds: latlng.toBounds(geolocationResult.coords.accuracy * 2),
        accuracy: geolocationResult.coords.accuracy,
        altitude: geolocationResult.coords.altitude,
        altitudeAccuracy: geolocationResult.coords.altitudeAccuracy || undefined,
        heading: geolocationResult.coords.heading,
        speed: geolocationResult.coords.speed,
        timestamp: geolocationResult.timestamp
      }
    }
  },

  _convertToErrorEventStandard (geolocationError) {
    // Convert the geolocation error from capacitor to a Leaflet ErrorEvent object:
    // https://leafletjs.com/reference-1.6.0.html#errorevent
    if (geolocationError?.code === geolocationError.TIMEOUT) {
      return geolocationError
    } else {
      let s = 'Geolocation error'
      try {
        s = '' + geolocationError
      } catch (_) {
        // the error cannot be converted to a string
      }
      return {
        code: -1,
        message: s
      }
    }
  },

  /**
   * Process and normalise compass events
   */
  _onDeviceOrientation: function (e) {
    if (!this._active) {
      return
    }

    let heading = null
    try {
      if (e.webkitCompassHeading) {
        // iOS
        heading = e.webkitCompassHeading
        if ('orientation' in window) {
          heading += window.orientation
        } else if (this._screenOrientationAngle) {
          heading += this._screenOrientationAngle
        }
        // custom onHeading function
        if (this.options.onHeading) {
          this.options.onHeading(heading)
        }
        this._setCompassHeading(heading)
      } else if (e.absolute && e.alpha) {
        // Android
        heading = 360 - e.alpha
        if ('orientation' in window) {
          heading += window.orientation
        } else if (this._screenOrientationAngle) {
          heading += this._screenOrientationAngle
        }
      }
    } catch (e) {
      // Some Android do not have a magnetometer so the event might be null
      console.warn('Cannot get heading: does the device has a magnetometer?')
      heading = null
    }

    // custom onHeading function
    if (this.options.onHeading) {
      this.options.onHeading(heading)
    }
    this._setCompassHeading(heading)
  },

  _onDeviceScreenOrientationChange: function (event) {
    // in degree
    this._screenOrientationAngle = event.target.screen.orientation.angle
  },

  _onCompassSuccess: function (heading) {
    if (!this._active) {
      return
    }
    if (this.options.onHeading) {
      this.options.onHeading(heading.magneticHeading)
    }
    this._setCompassHeading(heading.magneticHeading)
  },

  _onCompassError: function (error) {
    console.error('Compass error', error)
    if (this.options.onHeading) {
      this.options.onHeading(null)
    }
    this._setCompassHeading(null)
  },

  _onLocationPermissionDenied: function () {
    if (!this._active) {
      return
    }
    this._deactivate()
    this._updateContainerStyle()
    // check the error type: permission denied or geolocation services disabled
    Geolocation.checkPermissions().then(permissionStatus => {
      const isAppGeolocationPermissionDenied = permissionStatus.location === 'denied'
      this.options.onLocationPermissionDenied(isAppGeolocationPermissionDenied)
    }).catch(() => {
      // Safari does not support permissions API
      this.options.onLocationPermissionDenied(true)
    })
  }
})

L.control.mobileLocate = (options) => new L.Control.MobileLocate(options)
