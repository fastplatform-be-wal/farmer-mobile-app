import { $gettext } from 'src/boot/translate'

import { getSortedPlotPlantVarieties } from 'src/services/plot/plotFormatting'
import featureFlags from 'src/services/feature-flags'
import { apolloClient } from 'src/boot/apollo'
import queryPlotMapDetailsById from 'src/services/map/layers/graphql/queryPlotMapDetailsById'
import { getI18nField } from 'src/services/translate'
import { formatSurface } from 'src/services/display-surface-unit'

export const getDefaultOverlayPopUpContent = (event) => {
  let popUpContent = ''
  let properties = event.layer.properties
  if (properties == null || Object.keys(properties).length === 0) {
    // No properties attached to the layer, get the feature properties
    properties = event.feature ? event.feature.properties : {}
  }
  if (properties != null) {
    Object.keys(properties)
      .filter(property => (property !== 'id') && (property.substring(-3) !== '_id'))
      .forEach(property => {
        let value = properties[property]
        if (value && typeof (value) === 'string') {
          value = value.replace('\n', '<br>')
        }
        popUpContent += `<div style="margin-bottom: 10px;"><div class="text-grey text-capitalize">${property}</div><div>${value}</div></div>`
      })
  }
  return popUpContent
}

export const getAgriculturalPlotsPopupContent = ({ layer: { properties: { id, area } } }) => {
  let popUpContent = `<div class="text-subtitle1">${$gettext('Agricultural plot')}</div>`

  if (area) {
    const surfaceDisplay = formatSurface(area)
    popUpContent += `<div class="q-py-sm"><div class="text-grey text-capitalize">${$gettext('Area')}</div><div>${surfaceDisplay}</div></div>`
  }

  if (featureFlags.plotCanBeAdded() && id) {
    popUpContent += `
    <div class="full-width text-right">
      <a onclick="window.__fast_app.$router.push({name: 'plotCreate', params: { agriPlotId: ${id} }})"
         class="text-body2 q-py-xs q-mt-sm q-btn q-btn-item non-selectable no-outline q-btn--flat q-btn--rectangle text-primary">
        ${$gettext('Add to the current campaign')}
      </a>
    </div>`
  }
  return popUpContent
}

export function getPlotLayerPopUpDefaultContent ({ layer, feature }, options) {
  let popUpContent = ''
  let properties = layer.properties
  if (properties == null || Object.keys(properties).length === 0) {
    // No properties attached to the layer, get the feature properties
    properties = feature ? feature.properties : {}
  }
  if (properties != null) {
    const plotName = properties?.plot_name
    const plotId = properties?.id
    popUpContent += `<div class="text-body1">${plotName}</div>`
    if (options?.currentPlotId === plotId) {
      popUpContent += `<div class="full-width text-center text-caption">${$gettext('Currently selected')}</div>`
    } else {
      popUpContent += `<div class="full-width text-right"><a onclick="window.__fast_app.$router.push({name: 'plot', params: {idPlot: ${plotId}}})" class="text-body2 q-py-xs q-mt-sm q-btn q-btn-item non-selectable no-outline q-btn--flat q-btn--rectangle text-primary">${$gettext('View details')}</a></div>`
    }
    popUpContent += '<div class="full-width text-center"><svg focusable="false" fill="currentColor" width="20px" height="20px" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" class="q-spinner text-primary"><circle cx="15" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="60" cy="15" r="9" fill-opacity=".3"><animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from=".5" to=".5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite"></animate></circle><circle cx="105" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate></circle></svg></div>'
  } else {
    popUpContent += `<div class="text-body1">${$gettext('No data found')}</div>`
  }
  return popUpContent
}

export async function getPlotLayerPopUpAsynchronousContent ({ layer, feature }, options) {
  try {
    let properties = layer.properties
    if (properties == null || Object.keys(properties).length === 0) {
      // No properties attached to the layer, get the feature properties
      properties = feature ? feature.properties : {}
    }
    // get plot id from properties
    const plotId = properties.id
    const res = await apolloClient.query({
      query: queryPlotMapDetailsById,
      variables: {
        plotId
      },
      update: data => data.plot,
      fetchPolicy: 'network-only'
    })
    const plot = res?.data?.plot
    if (plot) {
      let popUpContent = ''
      // plot name
      if (plot.name || plot.authority_id) {
        popUpContent += `
          <div class="row items-end q-col-gutter-sm" style="max-width: min(70vw, 250px); min-width: min(60vw, 150px);">
            <div class="col text-body1 ellipsis">${plot.name ?? plot.authority_id}</div>
          </div>
        `
      }

      // plot area
      if (plot.area) {
        const surfaceDisplay = formatSurface(plot.area)
        popUpContent += `<div class="q-py-sm"><div class="text-body2 text-grey">${surfaceDisplay}</div>`
      }

      // plot plant species
      if (featureFlags.plotPlantVarietyCanBeDisplayed() && plot.plot_plant_varieties) {
        // eslint-disable-next-line camelcase
        const sortedPlotPlantVarieties = getSortedPlotPlantVarieties(plot.plot_plant_varieties)
        for (const plotPlantVariety of sortedPlotPlantVarieties) {
          let plantSpeciesName = getI18nField(plotPlantVariety?.plant_variety?.plant_species, 'name')
          if (sortedPlotPlantVarieties.length > 1 && plotPlantVariety?.percentage) {
            plantSpeciesName += `: ${plotPlantVariety.percentage}%`
          }
          if (plantSpeciesName) {
            popUpContent += `
              <div class="text-body2 text-grey ellipsis">
                <span>${plantSpeciesName}</span>
              </div>`
          }
        }
      }

      // actions:
      // - go to fertilization plan
      if (plot.fertilization_plans?.length) {
        // TODO Display the active plan if available rather than the first one in the list
        popUpContent += `<div class="full-width text-right"><a onclick="window.__fast_app.$router.push({name: 'fertilizationPlan', params: {fertilizationPlanId: ${plot.fertilization_plans[0].id}}})" class="text-body2 q-py-xs q-mt-sm q-btn q-btn-item non-selectable no-outline q-btn--flat q-btn--rectangle text-primary">${plot.fertilization_plans[0].name}</a></div>`
      }
      // - go to plot page
      if (options?.currentPlotId !== plotId) {
        popUpContent += `<div class="full-width text-right"><a onclick="window.__fast_app.$router.push({name: 'plot', params: {idPlot: ${plotId}}})" class="text-body2 q-py-xs q-mt-sm q-btn q-btn-item non-selectable no-outline q-btn--flat q-btn--rectangle text-primary">${$gettext('View details')}</a></div>`
      }
      return popUpContent
    } else {
      return getDefaultOverlayPopUpContent({ layer, feature })
    }
  } catch (e) {
    console.error('map', e)
    // default to the display of layer properties
    return getDefaultOverlayPopUpContent({ layer, feature })
  }
}
