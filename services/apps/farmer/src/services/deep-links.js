import { App } from '@capacitor/app'

// Listen for the 'appUrlOpen' event that is emitted when a user clicks on a deep link
export async function listenDeepLinks (router) {
  await App.addListener('appUrlOpen', ({ url }) => {
    // Example of url: https://app.fastplatform.eu/some/path
    // We extract the url path, in the above example "/some/path", and route the user to the corresponding page in the application.
    console.log('appUrlOpen: ' + url)
    const path = url.split('app.fastplatform.eu').pop()
    if (path) {
      // TODO reactivate deep links routing once it's useful (deactivated to avoid OAuth redirect issue)
      // router.push(path)
    }
    // else do nothing and continue normal routing (home page by default)
  })
}
