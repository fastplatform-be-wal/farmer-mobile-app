
import axios from 'axios'

import store from 'src/store'
import { persistor } from 'src/graphql/apollo/cache'
import { initSentry } from './sentry'
import { getRegionImageSrc } from 'src/utils/urls'
import { parseLanguageConfig } from 'src/services/translate'

const TOP_LEVEL_CONFIG_URL = 'conf/config.json'

async function getTopLevelConfig () {
  try {
    const { data } = await axios.get(TOP_LEVEL_CONFIG_URL)
    return data
  } catch (e) {
    // it should never happen because the config is a local resource
    console.error('Could not fetch top level config', e)
    return null
  }
}

function findRegionConfig (countriesAndRegionsConfig, regionId) {
  if (countriesAndRegionsConfig && Array.isArray(countriesAndRegionsConfig)) {
    for (const countryConfig of countriesAndRegionsConfig) {
      if (countryConfig?.region_id === regionId) {
        return countryConfig
      } else if (Array.isArray(countryConfig.regions)) {
        for (const regionConfig of countryConfig.regions) {
          if (regionConfig?.region_id === regionId) {
            return regionConfig
          }
        }
      }
    }
  }
  return null
}

function getLanguageConfig (countriesAndRegionsConfig, regionId) {
  if (countriesAndRegionsConfig && Array.isArray(countriesAndRegionsConfig)) {
    for (const countryConfig of countriesAndRegionsConfig) {
      if (countryConfig?.region_id === regionId) {
        return countryConfig.language_codes
      } else if (Array.isArray(countryConfig.regions)) {
        for (const regionConfig of countryConfig.regions) {
          if (regionConfig?.region_id === regionId) {
            return countryConfig.language_codes
          }
        }
      }
    }
  }
  return null
}

/**
 * Try to fetch config.json and put it in the store or fallback on the store config
 * @returns the top level config (local config.json) or throws error if no config is available
 */
export async function loadTopLevelConfig () {
  let topLevelConfig = await getTopLevelConfig()
  if (topLevelConfig) {
    store.commit('fastplatform/setTopLevelConfig', topLevelConfig)
  } else {
    topLevelConfig = store.state.fastplatform.topLevelConfig
    console.warn('Could not update top level config config')
  }
  if (!topLevelConfig) {
    throw new Error('Could not load top level config')
  }
  return topLevelConfig
}

/**
 * Fetch the countries and regions config
 * @param {Object} topLevelConfig the top level config
 * @returnsthe the countriesAndRegions config or throw error on failure
 */
export async function getCountriesAndRegionsConfig (topLevelConfig) {
  try {
    const endpoint = topLevelConfig.countriesAndRegionsConfigEndpoint
    const jsonFile = topLevelConfig.countriesAndRegionsConfigJSONFile
    let countriesAndRegionsConfigUrl = endpoint
    if (!endpoint?.endsWith('/')) {
      countriesAndRegionsConfigUrl += '/'
    }
    countriesAndRegionsConfigUrl += jsonFile
    const { data } = await axios.get(countriesAndRegionsConfigUrl)
    return data
  } catch (e) {
    console.error('Could not fetch countries and regions config', e)
    throw new Error('Countries and regions config is not available')
  }
}

/**
 * Try to get uo-to-date region config and save it in the store or fallback on the current store config
 * @param {Object} topLevelConfig the top level config
 * @param {String} regionId the region to update
 * @param {Boolean} isMobileApp mobile app flag
 * @returns the top level config (local config.json) or throws error if no config is available
 */
async function loadUpdatedRegionConfig (topLevelConfig, regionId, isMobileApp) {
  let config = null
  try {
    const countriesAndRegionsConfig = await getCountriesAndRegionsConfig(topLevelConfig)
    if (countriesAndRegionsConfig && Array.isArray(countriesAndRegionsConfig)) {
      config = findRegionConfig(countriesAndRegionsConfig, regionId)?.config
      if (config) {
        // Update the available languages
        const languageConfig = getLanguageConfig(countriesAndRegionsConfig, regionId)
        const availableLanguages = parseLanguageConfig(languageConfig)
        store.commit('fastplatform/updateAvailableLanguages', availableLanguages)

        // Update the region config login with endpoints from well-known configuration (10s timeout)
        const loginEndpoints = await getLoginEndpoints(config.login.wellKnownEndpoint, 10000)
        config.login = {
          ...config.login,
          ...loginEndpoints,
          clientId: isMobileApp ? config.login.appId : config.login.appIdDesktop
        }
        // Save updated config to the store
        store.commit('fastplatform/updateConfig', config)
        console.log('Refreshed region config for ' + regionId)
      }
    }
  } catch {
    // getCountriesAndRegionsConfig failed => fallback on store config
  }
  if (!config) {
    if (store.state.fastplatform.config) {
      console.warn('Could not update region config for ' + regionId + ': falling back on existing store config')
      config = store.state.fastplatform.config
    } else {
      throw new Error('Could not load region config')
    }
  }
  return config
}

/**
 * Get the OpenID login endpoints from the well-known configuration
 * @param {*} wellKnownEndpoint the region config well-known endpoint
 * @returns the loginEndpints
 */
async function getLoginEndpoints (wellKnownEndpoint, timeout) {
  try {
    // Request the well-known configuration to discover the login related endpoints
    const { data } = await axios.get(wellKnownEndpoint, { timeout })
    return data
  } catch (e) {
    if (store.getters['fastplatform/isOpenIDConfigValid']) {
      // Cannot discover the configuration but has a valid existing config: use existing config
      console.warn('Could not discover login endpoints: falling back on existing endpoints')
      return {}
    } else {
      // Cannot discover the configuration: use a OpenId default one based on the well known endpoint.
      console.warn('Could not discover login endpoints: falling back on OpenID default endpoints')
      const baseUrl = new URL(wellKnownEndpoint)
      return {
        authorization_endpoint: baseUrl.origin + '/openid/authorize',
        token_endpoint: baseUrl.origin + '/openid/token',
        userinfo_endpoint: baseUrl.origin + '/openid/userinfo'
      }
    }
  }
}

/**
 * Refresh the region-dependant configuration.
 * @param {Boolean} isMobileApp mobile app flag
 */
export async function refreshRegionConfig (isMobileApp) {
  try {
    const topLevelConfig = store.state.fastplatform.topLevelConfig
    const region = store.state.fastplatform.region
    if (topLevelConfig && region?.id) {
      console.log('Refreshing config for region ' + region.id + ' from ' + topLevelConfig.countriesAndRegionsConfigEndpoint + topLevelConfig.countriesAndRegionsConfigJSONFile)

      // Get and update region config
      await loadUpdatedRegionConfig(topLevelConfig, region.id, isMobileApp)

      // Restore the persisted Apollo cache.
      try {
        await persistor.restore()
      } catch (e) {
        console.error('Could not restore the Apollo persisted cache', e)
      }
    }
    // else no region selected yet => ignoring the config init
  } catch (e) {
    console.error('Could not initialize region configuration', e)
    throw new Error('Region config init failed')
  }
}

/**
 * Set the region config and apply changes to the language.
 * @param {Object} regionConfig the region definition from the countriesAndRegions config
 * @param {Array} availableLanguages the list of language codes available for the region
 * @param {Boolean} isMobileApp mobile app flag
 */
export async function setRegionConfig (regionConfig, availableLanguages, isMobileApp) {
  try {
    const region = { ...regionConfig }

    // Update the available languages
    store.commit('fastplatform/updateAvailableLanguages', availableLanguages)

    // Update the region config login with endpoints from well-known configuration (infinite timeout)
    const loginEndpoints = await getLoginEndpoints(region.config.login.wellKnownEndpoint, 0)
    region.config.login = {
      ...region.config.login,
      ...loginEndpoints,
      clientId: isMobileApp ? region.config.login.appId : region.config.login.appIdDesktop
    }
    store.commit('fastplatform/updateConfig', region.config)

    // Set the region in store (will trigger some initialization in App.js)
    store.commit('fastplatform/updateRegion', {
      id: region.region_id,
      name: region.name,
      image: getRegionImageSrc(region.image)
    })
    console.log('Set region config for ' + region.region_id)

    // Purge the persisted Apollo cache on region change
    await persistor.purge()

    // Initialize the error logger
    await initSentry()
  } catch (e) {
    console.error('Could not update region configuration', e)
    throw new Error('Region config update failed')
  }
}
