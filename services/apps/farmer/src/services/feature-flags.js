import store from 'src/store'

const featureFlags = {
  canInsertDemoHolding: function () {
    return store.state.fastplatform?.config?.features?.demoHolding
  },
  canSynchronizeHoldingAndHoldings: function () {
    return store.state.fastplatform?.config?.features?.iacsSynchronization
  },
  canCreateAndEditCustomFertilizer: function () {
    return store.state.fastplatform?.config?.features?.customFertilizer
  },
  canCreateTicket: function () {
    return store.state.fastplatform?.config?.features?.ticket
  },
  canDisplayBroadcast: function () {
    return store.state.fastplatform?.config?.features?.broadcast
  },
  canDisplayCustomFertilizer: function () {
    return store.state.fastplatform?.config?.features?.customFertilizer
  },
  canDisplayExternalAgriPlots: function () {
    return store.state.fastplatform?.config?.features?.externalAgriPlot
  },
  canDisplayExternalManagementRestrictionOrRegulationZone: function () {
    return store.state.fastplatform?.config?.features?.externalManagementRestrictionOrRegulationZone
  },
  canDisplayExternalProtectedSites: function () {
    return store.state.fastplatform?.config?.features?.externalProtectedSite
  },
  canDisplayExternalSurfaceWater: function () {
    return store.state.fastplatform?.config?.features?.externalSurfaceWater
  },
  canDisplayExternalWaterCourse: function () {
    return store.state.fastplatform?.config?.features?.externalWaterCourse
  },
  canDisplayGeotaggedPhoto: function () {
    return store.state.fastplatform?.config?.features?.geotaggedPhoto
  },
  canDisplayNitrogenLimitation: function () {
    return store.state.fastplatform?.config?.features?.nitrogenLimitation
  },
  canDisplayRegisteredFertilizer: function () {
    return store.state.fastplatform?.config?.features?.registeredFertilizer
  },
  canDisplayTicket: function () {
    return store.state.fastplatform?.config?.features?.ticket
  },
  canTakePhoto: function () {
    return store.state.fastplatform?.config?.features?.geotaggedPhoto
  },
  plotPlantVarietyCanBeAdded: function () {
    return store.state.fastplatform?.config?.features?.plotPlantVariety?.add
  },
  plotPlantVarietyCanBeDeleted: function () {
    return store.state.fastplatform?.config?.features?.plotPlantVariety?.delete
  },
  plotPlantVarietyCanBeDisplayed: function () {
    return store.state.fastplatform?.config?.features?.plotPlantVariety?.display
  },
  plotPlantVarietyCanBeEdited: function () {
    return store.state.fastplatform?.config?.features?.plotPlantVariety?.edit
  },
  plotPlantVarietyMultipleEnabled () {
    return store.state.fastplatform?.config?.features?.plotPlantVariety?.multiple
  },
  plotCanBeAdded: function () {
    return store.state.fastplatform?.config?.features?.plot?.add
  },
  plotCanBeDeleted: function () {
    return store.state.fastplatform?.config?.features?.plot?.delete
  },
  canDisplaySoilDerivedObject: function () {
    return store.state.fastplatform?.config?.features?.soilDerivedObject?.display
  },
  canDisplaySoilSite: function () {
    return store.state.fastplatform?.config?.features?.soilSite
  }
}

export default featureFlags
