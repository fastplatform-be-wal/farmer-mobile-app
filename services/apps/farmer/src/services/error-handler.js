import { Notify } from 'quasar'

import store from 'src/store'
import { $gettext } from 'src/boot/translate'

const MIN_NETWORK_ERROR_DISPLAY_INTERVAL = 180000

let router = null

export function initErrorHandler (routerInstance) {
  router = routerInstance
}

/**
 * Show network error notification no more than once every MIN_NETWORK_ERROR_DISPLAY_INTERVAL ms
 */
export function onNetworkError () {
  const now = Date.now()
  const lastNetworkErrorShownAt = store.state.appState.lastNetworkErrorShownAt
  const timeEllapsed = lastNetworkErrorShownAt ? (now - lastNetworkErrorShownAt) : Number.MAX_VALUE
  if (timeEllapsed > MIN_NETWORK_ERROR_DISPLAY_INTERVAL) {
    Notify.create({
      message: $gettext('You seem to be experiencing some network connectivity issues.'),
      color: 'negative',
      timeout: 5000
    })
    store.commit('appState/setLastNetworkErrorShownAt', now)
  }
}

/**
 * Logout user when user token has expired and can't be renewed
 */
export function onRefreshTokenError () {
  if (!router) {
    throw new Error('ErrorManager must be initialized with the router instance')
  }
  console.error('Could not refresh token: logging out user')
  // reset tokens config to avoid errors in logout
  store.commit('fastplatform/deleteTokens')
  if (router.currentRoute.name !== 'logout') {
    router.push({ name: 'logout' })
  }
}

/**
 * Logout user when config is invalid.
 */
export function onInvalidConfigError () {
  if (!router) {
    throw new Error('ErrorManager must be initialized with the router instance')
  }
  console.error('App config is invalid: logging out user')
  if (router.currentRoute.name !== 'logout') {
    router.push({ name: 'logout' })
  }
}
