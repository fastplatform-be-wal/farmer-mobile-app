import { Network } from '@capacitor/network'
import { PushNotifications } from '@capacitor/push-notifications'
import { Notify } from 'quasar'

import { apolloClient } from 'src/boot/apollo'
import store from 'src/store'
import { $gettext } from 'src/boot/translate'
import MUTATION_PUSH_NOTIFICATION_REGISTER_DEVICE from 'src/services/graphql/mutationPushNotificationRegisterDevice'
import MUTATION_PUSH_NOTIFICATION_UNREGISTER_DEVICE from 'src/services/graphql/mutationPushNotificationUnregisterDevice'

// minimum time interval before showing again to the user the notifications permission warning (30 days)
const MIN_NOTIFICATIONS_PERMISSION_WARNING_INTERVAL = 1000 * 3600 * 24 * 30

const REGISTRATION_STATUS = {
  NOT_REGISTERED: 'not registered yet',
  DO_NOT_REGISTER: 'user doesn\'t authorized notification (yet)',
  WAITING_NETWORK: 'waiting network connection to register',
  REGISTERED: 'device successfully registered'
}

class PushNotificationsService {
  deviceToken = null
  isNetworkAvailable = null
  listenersHandlers = {
    registration: null,
    registrationError: null,
    pushNotificationReceived: null,
    pushNotificationActionPerformed: null,
    networkStatusChange: null
  }

  networkType = null
  registrationStatus = REGISTRATION_STATUS.DO_NOT_REGISTER
  retryCounter = 0
  retryWaitConfig = {
    retryNumberBeforeIncreasingWait: 3, // after 3 retry start increasing waiting time between two retries
    waitTime: 3000, // 3 seconds
    maxWaitTime: 60000 * 5, // Max time to wait between two retries: 5 min
    hasReachedMaxWaitTime: false // is true if the waiting time between retries has reached the maximum value
  }

  username = null

  constructor (username) {
    this.username = username
  }

  _addPushNotificationListeners () {
    this.listenersHandlers.registration = PushNotifications.addListener('registration',
      (token) => {
        this.deviceToken = token.value
        apolloClient.mutate({
          mutation: MUTATION_PUSH_NOTIFICATION_REGISTER_DEVICE,
          variables: {
            user_id: this.username,
            device_token: token.value
          }
        }).then((res) => {
          console.log('[Push Notification] Device registered for notifications')
          // remove network listener. Keep the notification listeners to handle the incoming notifications
          this._removeNetworkListener()
          this.registrationStatus = REGISTRATION_STATUS.REGISTERED
        }).catch((err) => {
          console.error('[Push Notification] Could not register the device to the push notification service.', err)
          this.retry()
        })
      }
    )

    this.listenersHandlers.registrationError = PushNotifications.addListener('registrationError',
      (error) => {
        console.error('[Push Notification] Failed to register device: ' + JSON.stringify(error))
        this.retry()
      }
    )

    // Show us the notification payload if the app is open on our device
    this.listenersHandlers.pushNotificationReceived = PushNotifications.addListener('pushNotificationReceived',
      (notification) => {
        // store opening date to trigger unread messages refresh
        store.commit('appState/setLastPushNotificationOpenedAt', new Date())
        this._openPushNotification(notification)
      }
    )

    // Method called when tapping on a notification
    this.listenersHandlers.pushNotificationActionPerformed = PushNotifications.addListener('pushNotificationActionPerformed',
      (action) => {
        if (action.notification) {
          // store opening date to trigger unread messages refresh
          store.commit('appState/setLastPushNotificationOpenedAt', new Date())

          let { title, body } = action.notification
          if (!title && action.notification.data) {
            //* On Android the tap action notification seems to be nested into this data object for some reason
            title = action.notification.data.title
            body = action.notification.data.message
          }
          if (title) {
            // body may be empty
            const notification = {
              title,
              body
            }
            this._openPushNotification(notification)
          }
        }
      }
    )
  }

  _openPushNotification (notification) {
    Notify.create({
      type: 'info',
      message: notification.title,
      caption: notification.body,
      icon: 'las la-bullhorn',
      timeout: 0,
      group: false
    })
  }

  _getNetworkStatus () {
    return Network.getStatus()
  }

  getRegistrationStatus () {
    return this.registrationStatus
  }

  _isNetworkListenerAdded () {
    return this.listenersHandlers.networkStatusChange != null
  }

  _isPushNotificationListenersAdded () {
    return this.listenersHandlers.registration != null &&
      this.listenersHandlers.registrationError != null &&
      this.listenersHandlers.pushNotificationReceived != null &&
      this.listenersHandlers.pushNotificationActionPerformed != null
  }

  async _refreshNetworkStatus () {
    try {
      const networkStatus = await this._getNetworkStatus()
      this.isNetworkAvailable = networkStatus.connected
      this.networkType = networkStatus.connectionType
    } catch (err) {
      this.isNetworkAvailable = null
      this.networkType = null
    }
  }

  _registerDevice () {
    // Register with Apple / Google to receive push via APNS/FCM
    return PushNotifications.register()
  }

  registerDevice () {
    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    return new Promise((resolve, reject) => {
      PushNotifications.requestPermissions().then(async (permissionStatus) => {
        if (permissionStatus.receive === 'granted') {
          await this._refreshNetworkStatus()
          // start watching the network
          if (!this._isNetworkListenerAdded()) {
            this._watchNetwork()
          }
          // add push notification listeners
          if (!this._isPushNotificationListenersAdded()) {
            this._addPushNotificationListeners()
          }
          // Change the registering status
          this.registrationStatus = this.isNetworkAvailable ? REGISTRATION_STATUS.NOT_REGISTERED : REGISTRATION_STATUS.WAITING_NETWORK
          if (this.registrationStatus === REGISTRATION_STATUS.NOT_REGISTERED) {
            this._registerDevice()
          }
          // else wait for the network connection to be available and then register the device
          // (done in the network watcher directly)
          resolve()
        } else {
          console.warn('[Push Notification] User refused to receive notifications')
          reject('REFUSED')
        }
      }).catch(err => {
        console.error('[PUSH_NOTIFICATION] An error occurred while requesting permission for the push notification', err)
        reject('REQUEST_PERMISSION_FAILED')
      })
    })
  }

  _removeAllPushNotificationListeners () {
    PushNotifications.removeAllListeners()
  }

  _removeAllListeners () {
    this._removeAllPushNotificationListeners()
    this._removeNetworkListener()
  }

  removeAllDeliveredNotifications () {
    return PushNotifications.removeAllDeliveredNotifications()
  }

  _removeNetworkListener () {
    this.listenersHandlers.networkStatusChange.remove()
  }

  retry () {
    // retry mechanism
    let w = this.retryWaitConfig.waitTime
    this.retryCounter++
    if (this.retryCounter > this.retryWaitConfig.retryNumberBeforeIncreasingWait) {
      if (!this.retryWaitConfig.hasReachedMaxWaitTime) {
        // calculate the time to wait: min(maxWaitTime, x^y * 1000), where
        // x = time to wait between retries in seconds
        // y = retry counter - retryNumberBeforeIncreasingWait + 1  (+1 is required to start at pow 2)
        const t = Math.pow(this.retryWaitConfig.waitTime / 1000, this.retryCounter - this.retryWaitConfig.retryNumberBeforeIncreasingWait + 1) * 1000
        w = Math.min(this.retryWaitConfig.maxWaitTime, t)
        this.retryWaitConfig.hasReachedMaxWaitTime = t >= this.retryWaitConfig.maxWaitTime
      } else {
        // take the maximum time value
        w = this.retryWaitConfig.maxWaitTime
      }
    }
    // console.log(`[Push Notification] Registering device retry n°${this.retryCounter}`)
    setTimeout(() => {
      // Change the registering status
      this.registrationStatus = this.isNetworkAvailable ? REGISTRATION_STATUS.NOT_REGISTERED : REGISTRATION_STATUS.WAITING_NETWORK
      if (this.registrationStatus === REGISTRATION_STATUS.NOT_REGISTERED) {
        this._registerDevice()
      }
      // else wait for the network connection to be available and then register the device
      // (done in the network watcher directly)
    }, w)
  }

  unregisterDevice () {
    return new Promise((resolve, reject) => {
      if (this.deviceToken) {
        apolloClient.mutate({
          mutation: MUTATION_PUSH_NOTIFICATION_UNREGISTER_DEVICE,
          variables: {
            user_id: this.username,
            device_token: this.deviceToken
          }
        }).then(() => {
          console.log('[Push Notification] This device has been unregistered for notifications')
          this._removeAllListeners()
          this.deviceToken = null
          this.username = null
          this.registrationStatus = REGISTRATION_STATUS.NOT_REGISTERED
          resolve()
        }).catch((err) => {
          // TODO How to handle the error ?
          console.error('[Push Notification] Could not unregister the device on the push notification service', err)
          reject(err)
        })
      } else {
        // no device token. No "unregistration“ to perform
        resolve()
      }
    })
  }

  /**
   * Watch the network changes.
   * Register the device is waiting for network to be available.
   */
  async _watchNetwork () {
    this.listenersHandlers.networkStatusChange = await Network.addListener('networkStatusChange', (status) => {
      this.isNetworkAvailable = status.connected
      this.networkType = status.connectionType
      if (this.registrationStatus === REGISTRATION_STATUS.WAITING_NETWORK && this.isNetworkAvailable) {
        // Register the device
        this._registerDevice()
      }
    })
  }

  /**
   * Check and notify user if notifications are disabled.
   */
  checkPermissions () {
    PushNotifications.checkPermissions().then(permissionStatus => {
      const permissionState = permissionStatus.receive
      const now = Date.now()
      const lastNotificationsPermissionWarningShownAt = store.state.appState.lastNotificationsPermissionWarningShownAt
      if (permissionState === 'denied') {
        const timeEllapsed = lastNotificationsPermissionWarningShownAt ? (now - lastNotificationsPermissionWarningShownAt) : Number.MAX_VALUE
        if (timeEllapsed > MIN_NOTIFICATIONS_PERMISSION_WARNING_INTERVAL) {
          Notify.create({
            message: $gettext('Notifications are disabled: enable the notifications for FaST in your device settings to be nofified on new messages.'),
            icon: 'las la-exclamation-circle',
            timeout: 5000
          })
          // store last time the user was notified
          store.commit('appState/setLastNotificationsPermissionWarningShownAt', now)
        }
      } else if (permissionState === 'granted' && lastNotificationsPermissionWarningShownAt) {
        // reset the timer when notifications are granted
        store.commit('appState/setLastNotificationsPermissionWarningShownAt', null)
      }
    })
  }
}

/** The service instance accessd by the components */
export let pushNotificationServiceInstance = null

export function initializePushNotificationService () {
  const user = store.state.fastplatform.user
  pushNotificationServiceInstance = new PushNotificationsService(user.username)
}

export function stopPushNotificationService () {
  pushNotificationServiceInstance = null
}
