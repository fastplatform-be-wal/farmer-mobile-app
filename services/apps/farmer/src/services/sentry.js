import * as Sentry from '@sentry/capacitor'

import store from 'src/store'

export async function initSentry () {
  try {
    const sentryConfig = store.state.fastplatform.config?.sentry
    if (sentryConfig && sentryConfig?.enabled) {
      let version = process.env.APP_VERSION || ''
      version = version ? (version.startsWith('v') ? version : `v${version}`) : ''
      const release = `farmer-mobile-app@${version}`
      const environment = sentryConfig?.environment || ''

      const username = store.state.fastplatform.user.username
      let usernameHash = await crypto.subtle.digest('SHA-256', new TextEncoder('ascii').encode(username))
      usernameHash = Array.prototype.map.call(new Uint8Array(usernameHash), x => (('00' + x.toString(16)).slice(-2))).join('')

      Sentry.init({
        dsn: sentryConfig.uri,
        release,
        environment,
        enableAutoSessionTracking: false,
        integrations: function (integrations) {
          // integrations will be all default integrations
          return integrations.filter(function (integration) {
            return integration.name !== 'Breadcrumbs'
          })
        },
        beforeSend (event) {
          event.user = { username: usernameHash }
        }
      })
      console.log('Sentry initialized')
    } else {
      console.log('No Sentry configuration found')
    }
  } catch (err) {
    console.error('Failed to initialize Sentry: ', err)
  }
}
