This package contains some scripts to synchronize the package.json version and the capacitor apps version (iOS and 
Android)

It is based on the (capacitor-sync-version-cli)[https://github.com/bjesuiter/capacitor-sync-version-cli] github 
repositories.

The android script was modified to enable the support of the pre-release versions (eg. 0.1.1-1).
