const { updateAndroidVersion } = require('./src/android')
const { updateIosVersion } = require('./src/ios')
const readPkg = require('read-pkg')
const logdown = require('logdown')
const logger = logdown('Cap Sync Version')
logger.state = { isEnabled: true }

async function main () {
  const projectPackageJson = await readPkg()
  const packageVersion = projectPackageJson.version

  logger.log('Updating capacitor project versions to: ', packageVersion)

  await updateAndroidVersion(packageVersion)
  await updateIosVersion(packageVersion)
}

main().catch(error => {
  logger.error(error)
  console.log('\n')
})
