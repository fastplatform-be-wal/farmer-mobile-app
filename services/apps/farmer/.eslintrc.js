module.exports = {
  root: true,

  parserOptions: {
    parser: '@babel/eslint-parser',
    sourceType: 'module'
  },

  env: {
    browser: true,
    amd: true
  },

  extends: [
    'standard',
    'eslint:recommended',
    'plugin:vue/vue3-strongly-recommended',
    'plugin:quasar/standard'
  ],

  plugins: [
    'vue',
    'quasar'
  ],

  globals: {
    cordova: true,
    process: true
  },

  rules: {
    'generator-star-spacing': 'off',
    'arrow-parens': 'off',
    'one-var': 'off',

    'import/first': 'off',
    'import/named': 'error',
    'import/namespace': 'error',
    'import/default': 'error',
    'import/export': 'error',
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'import/no-extraneous-dependencies': 'off',
    'prefer-promise-reject-errors': 'off',

    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    'vue/multi-word-component-names': 'warn',
    camelcase: 'warn',
    'no-unsafe-optional-chaining': 'warn'
  }
}
