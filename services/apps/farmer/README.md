> This is the technical documentation of the FaST Farmer application.
You can also consult the main [FaST platform documentation](https://gitlab.com/fastplatform/docs).

# FaST Farmer application

The FaST Farmer app is an hybrid web and mobile application intended for farmers and agricultural advisors of the participating European countries and regions.

You will find an overview of the main features in the [reference documentation](https://gitlab.com/fastplatform/docs/-/blob/master/reference/mobile-and-web.md).

- [FaST Farmer application](#fast-farmer-application)
  - [Architecture](#architecture)
      - [Overview](#overview)
      - [Mobile and desktop layouts](#mobile-and-desktop-layouts)
      - [Data and APIs](#data-and-apis)
        - [GraphQL queries and subscriptions](#graphql-queries-and-subscriptions)
        - [Apollo cache and persistence](#apollo-cache-and-persistence)
      - [Mobile APIs](#mobile-apis)
      - [Components organisation](#components-organisation)
      - [Store](#store)
      - [Maps](#maps)
      - [Authentication](#authentication)
    - [Internationalization](#internationalization)
      - [Farm data synchronization](#farm-data-synchronization)
      - [App initialization](#app-initialization)
  - [Configuration](#configuration)
      - [Pathfinder mechanism](#pathfinder-mechanism)
      - [Configuration options](#configuration-options)
  - [Development](#development)
      - [Configuring the dev environment](#configuring-the-dev-environment)
        - [Prerequisites](#prerequisites)
          - [VSCode and plugins](#vscode-and-plugins)
          - [Browser tools](#browser-tools)
          - [Other utilities](#other-utilities)
          - [Certificates](#certificates)
      - [Installation](#installation)
        - [Configuration](#configuration-1)
      - [Running the dev server](#running-the-dev-server)
      - [Scripts](#scripts)
      - [Android development](#android-development)
      - [iOS development](#ios-development)
  - [Deployment](#deployment)
      - [Web app](#web-app)
      - [Mobile apps](#mobile-apps)
        - [Android Play Store](#android-play-store)
          - [Fastlane](#fastlane)
        - [iOS App Store](#ios-app-store)

## Architecture

The application is a [Vue.js](https://vuejs.org/) hybrid webapp. It can be deployed:
- as a *Single-Page Application* (SPA) for web browsers
- as a *Mobile App* (Android or iOS) through the [Capacitor](https://capacitorjs.com/) cross-platform native runtime

The web and mobile versions have the same features except for the mobile push notifications not implemented on the web version.

> The application relies on the FaST backend services to get data and perform calculations through the FaST platform API Gateway: it cannot function autonomously since no computations are made on-device.

#### Overview

The application is developped with the [Quasar](https://quasar.dev/) framework, an opensource Vue.js framework which provides the UI components and a CLI to facilitate cross-platform app development.

The project was bootstrapped with the Quasar CLI (the *Webpack* version) with the *SPA* and *Capacitor* *flavors* which provides the project [directory structure](https://quasar.dev/quasar-cli-webpack/).

As Quasar is built on top of Vue.js, the application also follow the main Vue.js guidelines.

> The application was initially developped using Vue.js 2 and then migrated to Vue.js 3. Nearly all of the components use the Options API instead of the newer Composition API.

To manage the application internal state, we used [Vuex](https://vuex.vuejs.org/), the state management library bundled with Vue.js.

The official router for Vue.js, [Vue Router](https://router.vuejs.org/) is used for navigation within the app.

Other frameworks of note include [Apollo client](https://www.apollographql.com/docs/) for GraphQL API calls, [Leaflet](https://leafletjs.com/) and plugins for maps, [Axios](https://github.com/axios/axios) for additional HTTP calls and [ESLint](https://eslint.org/) for linting and formatting.

> All of these frameworks are up-to-date (as of the 1st of April 2022) and used in a standard way.


#### Mobile and desktop layouts

The app adjusts automatically its layout according to the size of the user's screen:
- the *mobile layout* on small devices such as smartphone (the native mobile app only uses the mobile layout)
- the *desktop layout* on larger devices such as laptops

The differences between the two layouts are relatively minor:
- the navigation bar is at the bottom of the screen on mobile but on the top of the screen on desktop
- there is an extra top-level menu entry to manage the farms on desktop (only available on the left drawer on mobile)
- there are some extra titles on desktop
- some of the content is larger on desktop (such as maps)

#### Data and APIs

The following graph shows the Data/APIs interactions between the app and the backend services:

```plantuml
actor "User" as user
component "Farmer Application" as app <<SPA/Mobile app>>
component "API Gateway" as api_gateway <<Hasura>>
component "FaST Admin Portal" as portal <<Django>>
component "Map server" as maps <<MapProxy>>
component "Vector tiles server (private, external)" as vector_tiles <<pg_tileserv>>
storage "Satellite images" as satellite <<S3>>
storage "Media" as media <<Nginx>>

user -- app
app --> portal: Capacitor OAuth2 plugin (OIDC)
app --> api_gateway: Apollo client (GraphQL)
app --> maps: Leaflet
app --> vector_tiles: Leaflet VectorGrid
app --> satellite: Leaflet
app --> media: Axios
```

With some rare exceptions, all data API calls are made with the [Apollo client](https://www.apollographql.com/docs/) using the [GraphQL](https://graphql.org/) query language.

##### GraphQL queries and subscriptions

All GraphQL queries are declared in `.gql` or `.graphql` files because these files are compiled by the main FaST platform deploy mechanism into a GraphQL whitelist to check for allowed queries.

Most queries are made within the Vue components using the [Vue Apollo](https://apollo.vuejs.org/) library.

In the messaging feature in the Chat component for tickets we use [GraphQL subscriptions](https://www.apollographql.com/docs/react/data/subscriptions/) which are supported by Hasura.

##### Apollo cache and persistence

We use the [caching features](https://www.apollographql.com/docs/react/caching/cache-configuration/) from Apollo client to cache the GraphQL query results in order to render the views as fast as possible.

Most pages and components implement the `cache-and-network` fetch policy (configured as default) for the queries made with Apollo:
1. A loading spinner is shown to the user.
2. A first query is made to the cache: if it hits then the existing data is immediatly displayed.
3. A second query is made online: when it completes, the view is updated if the data are different from the cached one and the cache is updated with the latest data.

> Due to bugs in the Apollo client 3.5.X, the cache query is not immediatly returned when the `cache-and-network` policy is used. Setting the query parameter `notifyOnNetworkStatusChange` to `true` seems to fix this behavior.

In some cases where we want to be sure to get the lastest data from the server, we use the `network-only`fetch policy or the `no-cache` policy if there is a large amount of data we do not wish to store.

Since we use the Apollo `InMemoryCache` cache it is not persisted by default.
To persist the cache, we use the [apollo3-cache-persist](https://github.com/apollographql/apollo-cache-persist) library: each time the cache is updated, it is persisted to the browser IndexDB storage through the [localforage](https://github.com/localForage/localForage) library.
The cache is restored when the application is reloaded and purged at logout.

#### Mobile APIs

To access mobile-specific APIs we use these Capacitor plugins:
- `@capacitor/browser`: open an in-app browser
- `@capacitor/camera`: access the device camera (used to check permissions)
- `@capacitor/filesystem`: access the file system (used to store geotagged-photo and fertilization plans PDF)
- `@capacitor/geolocation`: access the device geolocation
- `@capacitor/keyboard`: customize the on-screen keyboard for iOS
- `@capacitor/network`: detect the network availability
- `@capacitor/push-notifications`: interact with push notifications
- `@capacitor/share`: save PDF files on iOS
- `@capacitor/splash-screen`: customize the app Spash screen
- `@capacitor/status-bar`: customize the app status bar

We use these Capacitor plugins from the community (which also provide web implementations):
- `@capacitor-community/camera-preview`: show the camera preview (web and mobile) inside a configurable component
- `@byteowls/capacitor-oauth2`: provide an OIDC client used for the web and mobile applications

We also use a few legacy Cordova plugins (compatible with Capacitor)
- `cordova-plugin-inappbrowser`: use the browser `window.open`feature
- `cordova-plugin-device-orientation`: detect the device orientation (heading) through the Web Browser API
- `cordova-plugin-screen-orientation`: detect the device screen orientation (portrait/landscape) through the Web Browser API


#### Components organisation

The project follows the Quasar CLI [directory structure](https://quasar.dev/quasar-cli-webpack/directory-structure).

Inside the root folder:
- `quasar.conf.js`: the Quasar CLI configuration file
- `Makefile`: a set of make commands to manage the application internationalization
- `.eslintrc.js`: the ESLint configuration file
- `BUILD`: the Bazel build file which uses the `nginx.conf` NGINX configuration file

Inside `public` (static files):
- `conf/`: the `config.json` and `pathfinder.json` folder
- `icons/`: the FaST icon in various sizes
- `images/`: static images

Inside `src`:
- `package.json`: dependencies and scripts
- `assets/`: asset images (bundled by Quasar CLI/Webpack)
- `boot/`: Quasar boot files (frameworks bootstrapping and defaults)
- `components/`: top-level UI components
- `css/`: SCSS files (bundled by Quasar CLI/Webpack)
- `font/`: fonts (bundled  by Quasar CLI/Webpack)
- `graphql/`: top-level GraphQL queries
- `layouts/`: top-level layouts
- `mixins/`: top-level mixins
- `pages/`: application pages, composed of `Pages` refered by the `Router` (contains subfolders `components`, `mixins` and `graphql`for components local to the `Page`)
- `router/`: Vue-router routes
- `services/`: JavaScript services (mutualized code used in different components)
- `store/`: Vuex store files
- `translate/`: message files (`po` format) for each locale
- `utils/`: JavaScript utilities`

Inside `src-capacitor`:
- `package.json`: Capacitor dependencies used to generate the native builds
- `android/`: Android-specific code generated by the Capacitor build
- `ios/`: iOS-specific code generated by the Capacitor build

Inside `dist`:
- `spa`: Built files for the web application (`index.html` entry point, staic assets includign JavaScript bundles)

#### Store

The Vuex store has 3 namespaced sub-stores:

- `app-state`: general UI state
- `fastplatform`: state related to the current FaST platform states (region configuration, current user, current holding campaign... )
- `fertilization-plan`: state of the fertilization plan being created/edited

The store state is persisted in the browser localstorage using the [vuex-persist](https://github.com/championswimmer/vuex-persist) library.

> The `vuex-persist` does not fully respect the Vue 3 breaking changes, therefore the Vuex tools are not functionnal in the Vue devtools Chrome extension.

The states are resetted when the user logs out.

#### Maps

All the maps in the application are displayed with the [Leaflet](https://leafletjs.com/) (version 1.7.1).
Most geographical data in the overlays is packaged as [Vector tiles ](https://wiki.openstreetmap.org/wiki/Vector_tiles) and served by the vector tiles server as protobuf files (`.pbf`): these files are parsed and displayed using the `vectorGrid.protobuf` layer from the Leaflet [VectorGrid](https://github.com/Leaflet/Leaflet.VectorGrid) plugin.
In addition, we use the [Leaflet locatecontrol](https://github.com/domoritz/leaflet-locatecontrol) plugin to add a *Locate me* control and the [Leaflet fullscreen](https://github.com/Leaflet/Leaflet.fullscreen) plugin to add a *Full screen* control.

The `BaseMap` component is the base Vue component for all maps. It is responsible for initializing the map with all the requested plugins and loading the base layers and overlays configuration.
There are various data sources for layers and overlays:
- base layers: these are raster layers such as *OpenStreetMap* displayed as Leaflet `tileLayer`
- base overlays: these are vector tiles layers displayed as VectorGrid `protobuf`layers (restricted zones, protected zones, water courses, water surfaces, agricultural plots and farm plots)
- additional overlays: these are additional WMS/TMS layers defined on the Admin Portal and displayed as Leaflet `tileLayer`
- satellite images overlay (RGB/NDVI): this is a special overlay displayed as a Leaflet `tileLayer` (the code is isolated in a `leaflet-fastplatform.js` special plugin file as it is also used to display RGB/NDVI overlays in the Django Admin Portal)

On top of the layers configuration, there is also some additional properties loaded when the application is started and saved in the store in the `fastplatform/portalConfiguration`: it contains some map default properties and overlay styles for the base overlays that override the default styles.

#### Authentication

To authenticate the user, we use the [Capacitor OAuth2 plugin](https://github.com/moberwasserlechner/capacitor-oauth2).
This plugin is used to authenticate the user using the OIDC protocol (OpenID Connect) with the Admin Portal serving as the identity provider.

### Internationalization

The application is internationalized with the [Vue 3 Gettext](https://github.com/jshmrtn/vue3-gettext) library.
The message strings are written in English (default language) inside of special `<translate>` tags and translated in the 9 supported languages (`fr, de, es, it, et, el, sk, ro, bg`) using the [Gettext](https://www.gnu.org/software/gettext/) approach.
The messages are extracted into `po` messages files located in the `translate/locale/`, translated and then compiled into a `translations.json` file used by the Vue 3 Gettext library.

#### Farm data synchronization

The application is able to synchronize the users' farm data through the Gateway API.
If the user has no active farm, then the application will show a start dialog to create a new farm and then synchronize the data.
When the application is loaded (or refreshed), it will navigate to the `HoldingsPage` where the farm data synchronization happens:
- if the user has an active holding campaign (saved in the store), then he will be redirected to the `HomePage`
- if he has farms, he can synchronize its farm (involving the remote IACS service) or create a demo farm
- if he syncs his farms, if he has only one farm, the current campaign will be automatically selected
- if he has multiple farms, he can select which farm to synchronize and start a new campaign

#### App initialization

This state diagram shows how the application is initialized:

```plantuml
[*] --> App : Application loaded
App : Refresh region config, portal configuration and init services
App --> Router : App initialized
Router : Check user and region config states
Router --> RegionSelectionPage: Invalid config (user not logged, invalid region config...)
RegionSelectionPage --> OauthLoginPage: Region selected
RegionSelectionPage : Load countries and regions configuration
OauthLoginPage : Load region config, portal configuration and init services
OauthLoginPage --> Router : Successful login
Router --> HoldingsPage : Valid config
HoldingsPage --> HomePage : Successful farm data initialization
HoldingsPage : Load farm data
HomePage --> [*]
HomePage : Display FaST home page
```

## Configuration

Since the codebase is the same for all regions, the application uses a configuration JSON file named `pathfinder.json` to customize the backend services URLs and the region-specific settings and features.

>The configuration supports multiple countries and regions in the same file which means the same application instance can connect to any region described in the configuration. However, after splitting the different regions during the FaST Year 2, the new deployment process expects only one region per app instance.

#### Pathfinder mechanism

The is the workflow for the configuration resolution:
1. For the **web application**: during the main FaST platform deployment, the `pathfinder` file is generated using the region properties: it will replace the default `pathfinder` file commited in the Farmer app repository.
  For the **mobile application**: as the deployment on the App stores is dissociated from the FaST platform deployment, the default `pathfinder` file will be used. Make sure it is correct!
2. When the application is loaded its fetches the configuration endpoint from the `public/conf/config.json`file (also called the *top level config*)
3. This `config.json` only contains the URL (endpoint and name) to the `pathfinder` file to use. This mechanism exists for legacy reasons as the `pathfinder` was deployed on a different server. Nowadays the default endpoint `public/conf/pathfinder.json`is always used (but may be overridden for development purposes).
4. The application loads the `pathfinder` file and displays the login options in the *Region selection* screen. If there are multiple region/countries entries in the file, the user must select a region: the selected region config is then loaded and stored until the next logout.

#### Configuration options

The file is a JSON list as it supports multiple countries.

*Example configuration for a country*
```json
  [{
    "name": "Slovensko", // the country name
    "enabled": true, // whether to display the country in the Region selection screen
    "image": "slovaquia.svg", // the country flag filename (served from public/conf/images/regions/)
    "language_codes": "sk,en", // the supported language codes (the first one in the default one)
    "region_id": "sk", // the region identifier
    "regions": [], // the country regions if available
    "config": { // the main config
      "contactEmail": "contact@fastplatform.eu", // the contact email of the support team
      "sentry": { // a configuration for the Sentry integration in GitLab
        "enabled": true,
        "environment": "beta",
        "uri": "https://glet_b327dea4b4c550f96df56f65011938e6@gitlab.com/api/v4/error_tracking/collector/27542591"
      },
      "apollo": { // the Apollo client configuration
        "fastplatform": {
          "httpLink": {
            "uri": "https://internal.api.beta.sk.fastplatform.eu/v1/graphql" // HTTPS endpoint for GraphQL requests
          },
          "wsLink": {
            "uri": "wss://internal.api.beta.sk.fastplatform.eu/v1/graphql" // WebSocket endpoint for GraphQL subscriptions
          }
        }
      },
      "features": { // Feature flags
        "broadcast": true, // Messaging broadcasts
        "customFertilizer": false, // Custom fertilizers features
        "demoHolding": true, // Demo farm creation
        "externalAgriPlot": true, // Map overlay with all agricultural plots
        "externalManagementRestrictionOrRegulationZone": true, // Map overlay with restricted zones (NVZ)
        "externalProtectedSite": true, // Map overlay with protected areas
        "externalSurfaceWater": false, // Map overlay with surface waters
        "externalWaterCourse": true, // Map overlay with water courses
        "geotaggedPhoto": true, // Photo features
        "iacsSynchronization": true, // Farm synchronization
        "nitrogenLimitation": true, // Nitrogen limitations
        "registeredFertilizer": false, // Registered fertilizers
        "ticket": true, // Messaging tickets
        "plotPlantVariety": { // Plot plant variety operations
          "add": true, // Add a new plant variety
          "delete": true, // Delete a plant variety
          "display": true, // Display the plant varieties
          "edit": true, // Edit the plant varieties
          "multiple": false // Allow multiple plant varities for a plot
        },
        "plot": { // Plot operations
          "add": true, // Add a new plot to a holding campaign
          "delete": true // Delete a plot from a holding campaign
        },
        "soilDerivedObject": {
          "display": true // Display the soil properties derived from the observations
        },
        "soilSite": true // Soil site features (soil samples)
      },
      "login": {
        "wellKnownEndpoint": "https://portal.beta.sk.fastplatform.eu/openid/.well-known/openid-configuration", // OpenID configuration endpoint
        "appId": "fastplatform-mobile", // application ID used in the mobile applications
        "appIdDesktop": "fastplatform" // application ID used in the SPA
      },
      "media": "https://media.beta.sk.fastplatform.eu/fastplatform-sk-private/media/beta", // Media server endpoint
      "mapServer": {
        "tileService": "https://map.beta.sk.fastplatform.eu/tiles" // Map server endpoint
      },
      "vectorTilesServers": {
        "fastplatform": "https://web-vector-tiles-fastplatform.beta.sk.fastplatform.eu", // Vector tiles server endpoint for the private data
        "external": "https://web-vector-tiles-external.beta.sk.fastplatform.eu" // Vector tiles server endpoint for the external/public data
      },
      "fertilizationAlgorithms": [ // List of supported fertilization algorithms
        {
          "id": "kalkulacky-hnojenie", // ID of the fertilizaiton algorithm (must correspond to the ID set in the backend)
          "name": "Kalkulacky Hnojenie" // Display name for the fertilization algorithm
        }
      ],
      "addOns": {
        "callbackBaseUrl": "https://portal.beta.sk.fastplatform.eu/add_ons/callback" // Base URL for add-ons callback
      },
      "photos": {
        "requiredLocationAccuracyInMeters": 50, // Minimum geolocation accuracy in meters required to take a photo
        "allowPhotosWithoutHeading": true // Allow taking a photo without detecting the device heading
      }
    }]
```

*Example configuration for a region*
```json
  {
    "name": "Belgique", // the country name
    "enabled": true, // whether to display the country in the Region selection screen
    "image": "belgium.svg", // the country flag filename (served from public/conf/images/regions/)
    "language_codes": "fr,de,en",  // the supported language codes (the first one in the default one)
    "regions": [ // a list of available regions for the country
      {
        "name": "Wallonie", // the region name
        "enabled": true, // whether to display the region in the Region selection screen
        "image": "wallonia.svg", // the region flag filename (served from public/conf/images/regions/)
        "region_id": "be-wal", // the region identifier
        "config": { // the main config (see above for details)
          //....
        }
      }
  }
```

## Development

#### Configuring the dev environment

##### Prerequisites

[Node.js](https://nodejs.) is required (version >= 14.0).
The latest LTS version is recommended (tested with Node.js 16.x LTS).

> The application can only be launched in *HTTPS* mode, even for the development server: you need valid certificates for the `localhost.fastplatform.eu` domain.

To run XCode and build the iOS app, you need a computer running MacOS.

###### VSCode and plugins

The application is developed with [VSCode](https://code.visualstudio.com/) (current version),
VSCode settings are commited to the `.vscode` folder in the root of the application.

The following extensions are strongly recommended:
- `Vetur`: the Vue tooling extension
- `ESLint`: the ESLint extension used for code linting and formatting

> Using the commited VSCode settings, the code is automatically formatted on save with the ESLint extension.

Other nice-to-have extensions include `Color Highlight`, `Auto Import`, `Better Comments`, `Edit CSV`, `EditorConfig`, `Markdown preview`, `npm Intellisense`, `Path Intellisense`, `Rainbow CSV`, `vscode-base64`, `Fix Irregular Whitespaces`, `gettext`, `GraphQL`, `Git Graph`, `Git History`.

###### Browser tools
- [Chrome devtools](https://www.google.com/chrome/): the Chrome devtools bundled with Chrome
- [Vue devtools](https://devtools.vuejs.org/guide/installation.html): official Vue devtools, very useful to visualize the Vue components and the Vuex state
- [Apollo devtools](https://www.apollographql.com/docs/react/development-testing/developer-tooling/): the Apollo client devtools to display the GraphQL queries and the cache content

###### Other utilities
To run the charts in the `README.md`, install [plantuml](https://plantuml.com/).

###### Certificates

To start the dev server (in HTTPS), you need to have the following valid SSL certificates in the `certificates/localhost.fastplatform.eu/` folder at the root of the project:
- `privkey1.pem`: the private key for the `localhost.fastplatform.eu` domain
- `cert1.pem`: the certificate for the `localhost.fastplatform.eu` domain
- `fullchain1.pem`: the CA chain for the `localhost.fastplatform.eu` domain
You can change the certificates names and paths by editing the `devServer` entry of the `quasar.conf.js` file.

#### Installation

```bash
# Install Quasar CLI globally
npm install -g @quasar/cli

# Install the dependencies
npm install

# (Optional) Install incongenie for generating icons
npm install -g @quasar/icongenie
```

##### Configuration

The application configuration relies on 2 JSON files in the `public/conf` folder:
- `config.json`: the top-level configuration file
- `pathfinder.json`: the local pathfinder configuration

You can modify the `config.json` fields `countriesAndRegionsConfigEndpoint` and/or `countriesAndRegionsConfigJSONFile` to load a remote `pathfinder` or another local one (useful in development). Be careful as the `config.json` will be commited by default (you can also set Git to ignore it).

> Keep in mind that the local pathfinder configuration will be used for the mobile app deployment!

#### Running the dev server
```bash
# Launch the Quasar CLI development server
npm run dev
```

#### Scripts

```bash
# Build the application for SPA, Android and iOS
npm run build

# Build the application for the SPA
npm run build:spa

# Build the application for Android
npm run build:android

# Build the application for iOS
npm run build:ios

# Synchronize the Capacitor dependencies (run it after adding a new Capacitor plugin)
npm run cap-synchronize

# Generate the icons for the application (icongenie needs to be globally installed)
npm run icongenie

# Verify the icons for the application (icongenie needs to be globally installed)
npm run icongenie:verify

# Ignore Git modifications on the public/conf/* files (useful if you tinker with the configuration files in development)
npm run git-ignore-conf-file-updates

# Resume tracking Git modifications on the public/conf/* files
npm run git-track-conf-file-updates

# Run the ESLint linter
npm run lint

# Fix the ESLint linter errors
npm run lint:fix

# Open the Android Studio project from the Android app folder (Android Studio needs to be installed)
npm run open-android-studio

# Open the XCode project from the iOS app folder (Xcode needs to be installed)
npm run open-xcode

# Sync Capacitor builds version with the package.json version
npm run sync-versions

# Build the application, sync the versions and run Fastlane beta for iOS and Android
npm run release-new-build
```

The scripts related to the internationalization with Gettext are located in the `Makefile`:

```bash
# Extract messages from source code into .po files
make messages

# Compile messages into translation.json file
make compile-messages

# Use gettext function to extract untranslated strings from app.po file into untranslated.po file
make extract-untranslated-messages

# Merge newly translated .po file with existing app.po file
make merge-translated-po-messages

# Convert .po file to .csv using po-csv node tool
make messages-convert-po-to-csv-node

# Convert .csv file to .po using po-csv node tool
make messages-convert-csv-to-po-node
```

#### Android development

Install the [Android Studio](https://developer.android.com/studio/install.html) (latest version recommended).

To use your own Android device, you need to enable the *USB debugging* option in the [dev options](https://developer.android.com/studio/debug/dev-options#enable).

After building the Android app, you can open it by running the `open-android-studio` script and run the application with the Simulator or your device.

#### iOS development

Install [Xcode](https://developer.apple.com/xcode/) (latest version recommended, MacOS is required).

To use your own iOS device, you need to register it with the Apple Developer Portal and link it to your Apple Developer account.

You need to create a development profile for the App to be able to run the application on your device: to do this, you can use the Fastlane tool (see the [iOS App Store](#ios-app-store) section).

After building the iOS app, you can open it by running the `open-xcode` script and run the application with the Simulator or your device.

## Deployment

The web and mobile deployment are completely separate processes.

#### Web app

The web app is deployed as a `SPA` behind a `Nginx` server.
The binaries are first built using Quasar and packaged into a container usint the automoation tool `Bazel` and then deployed along with the backend services.

You will find more details on the overall deployment process in the general [documentation](https://gitlab.com/fastplatform/docs/).

#### Mobile apps

To deploy the mobile apps, we use a *manual process* distinct from the main deployment process.

Most of the steps to deploy Android and iOS apps can be automated using [Fastlane](https://fastlane.tools/).
Fastlane is a tool that automates the steps required to release an application to a store, such as taking screenshots,
signing the application and pushing a new version to the store.

> The application management on the *Android Play Store* and the *iOS App Store* is not covered in this documentation.

##### Android Play Store

1. You will need a **Google developer account** and to pay the registration fee .
2. Create the **Fast Farmer** application on the Google Play Console and configure the necessary information (a Privacy Policy is required).
3. As the app **package name** must be unique, if is not the default `eu.fastplatform.mobile.farmer`, make sure to update the `applicationId` in the `build.gradle` file (see [instructions](https://capacitorjs.com/docs/android/configuration#changing-the-package-id)). For example, your package name could be `eu.fastplatform.mobile.farmer.XX` where `XX` is the country/region code.
4. You need to generate a **keystore** and an **alias key** to sign your builds and put the keystore file `fast-farmer-app-release-key.keystore` in the `src-capacitor/android/secrets` folder. The alias key should be named `fast-farmer-app-key-alias` and its password should be the same as the keystore password (detailed instructions are available on the [Android guide](https://developer.android.com/studio/publish/app-signing#generate-key)).
5. You need to get the **Google API key** as a JSON file containing the Google credentials for the application and save it as `google-api-key.json` in the `src-capacitor/android/secrets` folder: the API key is what allow Fastlane to communicate with your Google Play Console account and your application (detailed instructions are available on the [Fastlane guide: Collect your Google credentials](https://docs.fastlane.tools/getting-started/android/setup/)).
6. You need to generate a **Firebase Android** JSON configuration file named `google-services.json` and put it in the `src-capacitor/android/app` folder: thio allows the app to use the Firebase Cloud messaging services for Push Notifications (required). To do so you must add Firebase to your Android project and download the configuration file from the Firebase console (detailed instructions are available on the [Firebase Android setup guide](https://firebase.google.com/docs/android/setup)).
7. The **app version** must be unique: if you already uploaded a build with the same version, you need to up the main version in the `package.json` file, use the `npm run sync-versions` script to update the Capacitor files version then rebuild the app with `npm run build:android`.
8. Make sure the `public/conf/pathfinder.json` file contains the **correct configuration** for your application.
9.  Create a **signed bundle** release with Android Studio (a release build) with your keystore and key alias (detailed instructions are available on the [Android app signin guide](https://developer.android.com/studio/publish/app-signing)).
10. On the Google Play console, you can then **create a new release** for *Internal testing* and upload your signed bundle `.aab` file.

> Do not commit unencrypted Android secrets to the repository: the `fast-farmer-app-release-key.keystore`, `google-api-key.json` and `google-services.json` contain secrets that are linked to your account and must not be shared with the public.


###### Fastlane

To setup Fastlane for Android, follow the [Android getting started guide](https://docs.fastlane.tools/getting-started/android/setup/).

Make sure the `package_name` for the Android Fastlane `Appfile` correspond to your `applicationId` in the `build.gradle` file.

> You must first manually upload a build to the Google Play Store before using Fastlane because Fastlane needs to extract the metadata from the build such as the unique package name.

Fastlane commands to run in the `src-capacitor/android` folder:
```bash
# Retrieve the metadata from the Google Play Console (only needed if you do not have a metadata folder in the Fastlane folder)
# First a build must be manually uploaded to the Play Store for this command to work
fastlane supply init

# Build and deploy the application on the Google Play Console either as an Internal or Beta release depending on the configuration of the Fastfile
# This command will ask you for the keystore and alias key passwords
fastlane beta

# Build and release a production version
fastlane deploy
```

##### iOS App Store

To setup Fastlane for iOS, follow the [iOS getting started guide](https://docs.fastlane.tools/getting-started/ios/setup/).

1. You will need an **Apple Developer account** and to pay the registration fee.
2. Create the **Fast Farmer** application on the App Store and configure the necessary information.
3. As the app **bundle ID** must be unique, if is not the default `eu.fastplatform.mobile.farmer-app`, make sure to update the `applicationId` in the `build.gradle` file and the iOS Fastlane `Appfile` (see [instructions](https://quasar.dev/quasar-cli-webpack/developing-capacitor-apps)). For example, your package name could be `eu.fastplatform.mobile.farmer-app.XX` where `XX` is the country/region code.
4. **Certificates and provisioning profiles** matching your working environment (appstore, development...): using the Fastlane `match` command you can create a provisioning profile for your environment and sync it on a Git repository.
5. The **app version** should be unique (although you can upload multiple iOS builds with the same version): if you already uploaded a build with the same version, shoul up the main version in the `package.json` file, use the `npm run sync-versions` script to update the Capacitor files version then rebuild the app with `npm run build:android`.
6. Make sure the `public/conf/pathfinder.json` file contains the **correct configuration** for your application.
7. Deploy the app in testing with the `fastlane beta` command.

Fastlane commands to run in the `src-capacitor/ios/App` folder:
```bash
# Create or retrieve the certificates and provisioning profiles for development (development profiles expire after 1 year)
fastlane match development

# Create or retrieve the certificates and provisioning profiles for the appstore
fastlane match appstore

# Destroy an expired provisioning profile (it will also delete the files in the repository)
fastlane match nuke <environment> # (development, appstore)

# Build and deploy the application to TestFlight using the appstore environment (you will need to login with your Apple Developer account)
fastlane beta

# Build and release a production version
fastlane deploy
```

After uploading a build to the store, you need to login on the App Store, set up the proper authorizations and publish the new build.
