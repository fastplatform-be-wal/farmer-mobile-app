load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@build_bazel_rules_nodejs//:index.bzl", "node_repositories", "npm_install")
load("@io_bazel_rules_docker//container:container.bzl", "container_pull")
load("@io_bazel_rules_docker//nodejs:image.bzl", _nodejs_image_repos = "repositories")
load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")


def transitive_deps():
    _nodejs_image_repos()

def docker_deps():
    container_deps()
    maybe(
        container_pull,
        name = "nginx_image_base",
        registry = "index.docker.io",
        repository = "nginx",
        digest = "sha256:966f134cf5ddeb12a56ede0f40fff754c0c0a749182295125f01a83957391d84", # alpine
    )
    
def npm_deps():
    node_repositories(
        node_version = "16.14.0",
        node_repositories = {
            "16.14.0-darwin_arm64": ("node-v16.14.0-darwin-arm64.tar.gz", "node-v16.14.0-darwin-arm64", "56e547d22bc7be8aa40c8cfd604c156a5bcf8692f643ec1801c1fa2390498542"),
            "16.14.0-darwin_amd64": ("node-v16.14.0-darwin-x64.tar.gz", "node-v16.14.0-darwin-x64", "26702ab17903ad1ea4e13133bd423c1176db3384b8cf08559d385817c9ca58dc"),
            "16.14.0-linux_arm64": ("node-v16.14.0-linux-arm64.tar.xz", "node-v16.14.0-linux-arm64", "5a6e818c302527a4b1cdf61d3188408c8a3e4a1bbca1e3f836c93ea8469826ce"),
            "16.14.0-linux_ppc64le": ("node-v16.14.0-linux-ppc64le.tar.xz", "node-v16.14.0-linux-ppc64le", "d1012696cacb3833b8b33748905e716f2524766b29a2a4c405f34ed2f3e5fdb4"),
            "16.14.0-linux_s390x": ("node-v16.14.0-linux-s390x.tar.xz", "node-v16.14.0-linux-s390x", "1c98494bc097547bcadb972780aec58bb40b9c094f0daed75debfee4cb980fd9"),
            "16.14.0-linux_amd64": ("node-v16.14.0-linux-x64.tar.xz", "node-v16.14.0-linux-x64", "0570b9354959f651b814e56a4ce98d4a067bf2385b9a0e6be075739bc65b0fae"),
            "16.14.0-windows_amd64": ("node-v16.14.0-win-x64.zip", "node-v16.14.0-win-x64", "c783f32aa22758e9fdcabb23daf348cc52f876fbd679d54edc2c4921ccd6d6c5"),
        },
    )
    maybe(
        npm_install,
        name = "apps_farmer_npm",
        package_json = "//services/apps/farmer:package.json",
        package_lock_json = "//services/apps/farmer:package-lock.json",
    )

def deps_step_1():
    transitive_deps()
    docker_deps()
    npm_deps()
